package View;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

import org.openstreetmap.gui.jmapviewer.MapMarkerCircle;

import Controller.RegistrosCenso;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;

public class ManejoDeCensistas extends JDialog {

	private static final long serialVersionUID = 1L;
	private RegistrosCenso registros;
	private JTextPane panelDeCensistas;
	private JTextField nombreCensistaIngresado;
	private JComboBox<String> censistasRegistrados;

	private Map <String,Set<MapMarkerCircle>> conjManzanasPorCensista;
	private boolean seEliminoUnCensistaYaAsignado;
	
	/**
	 * Muestra este objeto.
	 */
	public void mostrar() 
	{
		setVisible(true);
	}	
	
	/**
	 * Oculta este objeto.
	 */
	public void ocultar() 
	{
		setVisible(false);
	}

	/**
	 * Crea la ventana y carga los datos pasados por par�metro.
	 * @param frame: corresponde a la ventana de retorno.
	 * @param registros: corresponde a los registros del radio censal.
	 */
	public ManejoDeCensistas(JFrame frame, RegistrosCenso registros, Map <String,Set<MapMarkerCircle>> conjManzanasPorCensista) 
	{
		super(frame, true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ManejoDeCensistas.class.getResource("/Imagenes/Censistas.png")));
		
		try 
		{
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} 
		catch(Exception e) 
		{ 
			e.printStackTrace();		
		} 
		
		setTitle("Censistas");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.registros = registros;
		
		this.conjManzanasPorCensista = conjManzanasPorCensista;

		initialize();
	}
	
	private void initialize() 
	{
		
		//CONFIGURACI�N
		getContentPane().setLayout(null);
		setBounds(521, 133, 360, 455);
		setAlwaysOnTop(true);
		setModal(true);
		configurarCuadroDeTexto();
		
		//COMPONENTES
		crearTitulo();
		crearIngresoDeNuevosCensistas();
		crearEliminacionDeCensistas();
		crearEtiquetasAlmacenarYEliminarCensistas();
		crearBotonFinalizar();
	}


	/**
	 * Configura el cuadro de texto en la ventana.
	 */
	private void configurarCuadroDeTexto() 
	{
		panelDeCensistas = new JTextPane();
		panelDeCensistas.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 14));
		panelDeCensistas.setEditable(false);
		panelDeCensistas.setOpaque(true);
		panelDeCensistas.setBounds(1, 1, 580, 539);
		cargarCensistasEnTextPane();
		getContentPane().add(panelDeCensistas);
		
		JScrollPane scrollPane = new JScrollPane(panelDeCensistas);
		
		scrollPane.setBounds(10, 53, 322, 228);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setOpaque(false);
		scrollPane.getViewport().setOpaque(false);
		super.getContentPane().add(scrollPane);
	}
	
	/**
	 * Muestra los censistas en la pantalla.
	 */
	private void cargarCensistasEnTextPane() 
	{
		Set<String> censistas = registros.darNombresDeCensistas();
		StringBuilder sb = new StringBuilder();
		for(String censista : censistas)
			sb.append(censista + "\n");
		panelDeCensistas.setText(sb.toString());	
	}

	/**
	 * Crea el t�tulo de la pantalla.
	 */
	private void crearTitulo() 
	{
		JLabel lblTitulo = new JLabel("Censistas Registrados");
		lblTitulo.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 16));
		lblTitulo.setBounds(10, 5, 318, 51);
		getContentPane().add(lblTitulo);
	}
	
	/**
	 * Crea el JTextField y el bot�n para poder ingresar nuevos censistas.
	 * El boton OK sirve para enviar lo ingresado por el usuario, pero 
	 * el censista tambi�n se almacena si se aprieta la tecla enter.
	 */
	private void crearIngresoDeNuevosCensistas() 
	{
		nombreCensistaIngresado = new JTextField();
		nombreCensistaIngresado.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		nombreCensistaIngresado.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyPressed(KeyEvent e) 
			{
				if(e.getKeyCode() == KeyEvent.VK_ENTER) 
				{	//si aprieta la tecla enter...
					String nombreIngresado = nombreCensistaIngresado.getText();
					almacenarCensista(nombreIngresado);
					nombreCensistaIngresado.setText("");
				}
			}
		});
		nombreCensistaIngresado.setBounds(117, 302, 152, 22);
		nombreCensistaIngresado.requestFocus();
		getContentPane().add(nombreCensistaIngresado);
		nombreCensistaIngresado.setColumns(10);
		
		JButton botonOkNuevoCensista = new JButton("Ok");
		botonOkNuevoCensista.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String nombreIngresado = nombreCensistaIngresado.getText();
				almacenarCensista(nombreIngresado);
				nombreCensistaIngresado.setText("");
				nombreCensistaIngresado.requestFocus();
			}
		});
		botonOkNuevoCensista.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		botonOkNuevoCensista.setBounds(279, 302, 55, 23);
		getContentPane().add(botonOkNuevoCensista);
	}
	
	/**
	 * Almacena el censista en los registros del censo.
	 * @param censista corresponde al nombre del censista a almacenar.
	 */
	private void almacenarCensista(String censista) 
	{
		try
		{
			registros.registrarCensista(censista);
			actualizarCensistasEnPantalla();
			
		} 
		catch(Exception e) 
		{
			JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", 0);
		}
	}
	
	/**
	 * Muestra los censistas en la pantalla y en el combo box para eliminarlos.
	 */
	private void actualizarCensistasEnPantalla() 
	{
		cargarCensistasEnTextPane();
		cargarCensistasEnComboBox(censistasRegistrados);
	}
	
	/**
	 * Crea el combo box y el bot�n OK para eliminar censistas registrados.
	 */
	private void crearEliminacionDeCensistas() 
	{
		censistasRegistrados = new JComboBox<String>();
		censistasRegistrados.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		censistasRegistrados.setBounds(117, 335, 152, 22);
		cargarCensistasEnComboBox(censistasRegistrados);
		getContentPane().add(censistasRegistrados);
		
		JButton botonOkEliminarCensista = new JButton("Ok");
		botonOkEliminarCensista.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		botonOkEliminarCensista.setBounds(279, 336, 55, 23);
		botonOkEliminarCensista.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String censista = censistasRegistrados.getSelectedItem().toString();
				if(censista != null) 
				{
					registros.eliminarCensistaRegistrado(censista);
					
					if(conjManzanasPorCensista != null && conjManzanasPorCensista.containsKey(censista))
					{
						seEliminoUnCensistaYaAsignado = true;
					}
					panelDeCensistas.removeAll();
					actualizarCensistasEnPantalla();
				}	
			}
		});
		getContentPane().add(botonOkEliminarCensista);
	}
	
	/**
	 * Crea las etiquetas que informan el uso de los componentes de la ventana.
	 */
	private void crearEtiquetasAlmacenarYEliminarCensistas() 
	{
		//NUEVO CENSISTA
		JLabel lblNuevoCensista = new JLabel("Nuevo censista:");
		lblNuevoCensista.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblNuevoCensista.setBounds(10, 302, 117, 22);
		getContentPane().add(lblNuevoCensista);
		
		//ELIMINAR CENSISTA
		JLabel lblEliminarCensista = new JLabel("Eliminar censista:");
		lblEliminarCensista.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEliminarCensista.setBounds(10, 338, 117, 14);
		getContentPane().add(lblEliminarCensista);
	}
	
	/**
	 * Eval�a si se elimino un censista con asignaci�n de manzanas.
	 * @return true si se elimino un censista con asignacio�n y false en caso contrario.
	 */
	public boolean seEliminoCensistaAsignado() 
	{
		return seEliminoUnCensistaYaAsignado;
	}

	private void aplicar(Set<String> censistas, Consumer<String> consumidor)
	{
		for(String censista : censistas)
			consumidor.accept(censista);
	}
	
	/**
	 * Carga los censistas del censo en un combo box.
	 * @param comboBox corresponde al combo box a completar.
	 */
	private void cargarCensistasEnComboBox(JComboBox<String> comboBox) 
	{
		comboBox.removeAllItems();
		if(!registros.darNombresDeCensistas().isEmpty()) 
		{
			comboBox.setEnabled(true);
			aplicar(registros.darNombresDeCensistas(), c -> comboBox.addItem(c));
		}
		else
			comboBox.setEnabled(false);
	}
	
	/**
	 * Crea un bot�n que oculta la ventana.
	 */
	private void crearBotonFinalizar() 
	{
		JButton botonFinalizar = new JButton("Finalizar");
		botonFinalizar.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		botonFinalizar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ocultar();
			}
		});
		botonFinalizar.setBounds(113, 371, 129, 23);
		getContentPane().add(botonFinalizar);
	}
}