package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


public class HerramientasInterfaz {

	/**
	 * Crea un bot�n que no es utilizado en la pantalla inicial de la aplicaci�n.
	 * @param titulo: corresponde al texto que se mostrar� en el bot�n.
	 * @param x: corresponde al valor x de la coordenada del bot�n dentro de la ventana.
	 * @param y: corresponde al valor y de la coordenada del bot�n dentro de la ventana.
	 * @return JButton creado con estos datos, con tama�o, color y fuente predeterminada.
	 */
	public static JButton crearBotonSecundarioPredeterminado(String titulo, int x, int y) 
	{
		JButton button = new JButton(titulo);
		button.setBounds(x, y, 215, 23);
		button.setForeground(new Color(0, 0, 51));
		button.setBackground(new Color(0, 102, 204));
		button.setFont(new Font("Ebrima", Font.PLAIN, 13));
		return button;
	}
	
	/**
	 * Crea un bot�n que es usado en la pantalla inicial de la aplicaci�n.
	 * @param titulo: corresponde al texto que se mostrar� en el bot�n.
	 * @return JButton creado con este dato, con fuente y color predeterminado.
	 */
	public static JButton crearBotonPantallaInicial(String titulo) 
	{
		JButton button = new JButton(titulo);
		button.setForeground(new Color(0, 0, 51));
		button.setBackground(new Color(0, 0, 102));
		button.setFont(new Font("Ebrima", Font.BOLD, 15));
		return button;
	}
	
	/**
	 * Devuelve la fuente utilizada en los cuadros de di�logo simples.
	 */
	public static Font fuenteCuadrosDeDialogo() 
	{
		return new Font("Microsoft YaHei UI", Font.PLAIN, 12);
	}
	
	public static Font fuentePrincipal() {
		return new Font("Ebrima", Font.PLAIN, 13);
	}
	
	/**
	 * Crea una barra de herramientas con preguntas frecuentes de los usuarios.
	 * @param pantallaInicial: corresponde a la pantalla donde se crea esta
	 * barra de herramientas.
	 */
	public static void crearBarraHerramientas(JFrame pantallaInicial) 
	{
		JMenu preguntas = new JMenu("Preguntas frecuentes");
		String queEs = "�En qu� consiste la asignaci�n de radios censales?";
		String comoFunciona = "�C�mo funciona la asignaci�n?";
		JMenuItem pregunta1 = new JMenuItem(queEs);

		pregunta1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				CuadroDialogo pregQueEs = new CuadroDialogo(pantallaInicial, queEs,
						HerramientasInterfaz.respuestPreg1());
				tituloEiconoJDialog(pregQueEs);
				pregQueEs.setVisible(true);
			}
		});
		preguntas.add(pregunta1);
		
		JMenuItem pregunta2 = new JMenuItem(comoFunciona);
		pregunta2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				CuadroDialogo pregComo = new CuadroDialogo(pantallaInicial, comoFunciona, 
						HerramientasInterfaz.respuestPreg2());
				tituloEiconoJDialog(pregComo);
				pregComo.setVisible(true);
			}
		});
		preguntas.add(pregunta2);
		
		JMenuBar barraDeMenu = new JMenuBar();
		barraDeMenu.add(preguntas);
		pantallaInicial.setJMenuBar(barraDeMenu);
	}
	
	/**
	 * Setea �cono y t�tulo predeterminado para preguntas frecuentes
	 * al cuadro pasado por par�metro.
	 */
	private static void tituloEiconoJDialog(JDialog cuadro) 
	{
		cuadro.setTitle("Preguntas Frecuentes");
	}
	
	/**
	 * Devuelve la respuesta a la pregunta �En qu� consiste la asignaci�n de radios censales?.
	 */
	private static String respuestPreg1() 
	{
		String resp = "Es una aplicaci�n que permite armar un radio censal en el mapa y "
				+ "registrar censistas, para luego asignarles manzanas.\n"
				+ "Busca asignar la menor cantidad de censistas en la zona marcada, "
				+ "con una cantidad de hasta tres manzanas por cada uno de ellos.";
		return resp;
	}

	/**
	 * Devuelve la respuesta a la pregunta �C�mo funciona la asingnaci�n?
	 */
	private static String respuestPreg2() 
	{
		String resp = "La aplicaci�n permite: \n"
				+ "-	Agregar manzanas en el mapa, para esto se debe apuntar en la "
				+ "ubicaci�n correspondiente.\n"
				+ "-	Eliminar manzanas registradas apretando �Eliminar manzana� y eligiendo la manzana deseada.\n"
				+ "-	Establecer la contiguidad entre dos manzanas registradas apretando �Establecer Contiguidad� "
				+ "y eligiendo las opciones deseadas.\n"
				+ "-	Eliminar una contiguidad registrada apretando �Eliminar Contiguidad� y eligiendo las "
				+ "manzanas correspondientes.\n"
				+ "-	Registrar y eliminar censistas a trav�s de �Censistas�.\n"
				+ "Los censistas se registran ingresando su nombre y presionando la tecla enter o el bot�n OK. "
				+ "Se eliminan seleccionando el censista a eliminar en el men� desplegable y presionando el bot�n OK.\n"
				+ "-	Asignar los censistas registrados a las manzanas del radio censal.\n"
				+ "-	Guardar y recuperar el �ltimo registro del censo.\n";
		return resp;
	}
}