package View;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import Controller.RegistrosCenso;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;


public class VentanaConectarManzanas extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private RegistrosCenso registros;
	private boolean fueEnviado; //True si se presiono el bot�n ENVIAR, False en caso contrario.
	private Integer primerManzana;
	private Integer segundaManzana;
	private JFrame frame;

	/**
	 * Crea la ventana y carga los datos pasados por par�metro.
	 * @param frame: corresponde a la ventana de retorno.
	 * @param registros: corresponde a los registros del radio censal.
	 */
	public VentanaConectarManzanas(JFrame frame, RegistrosCenso registros) 
	{
		super(frame, true);
		
		try 
		{
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} 
		catch(Exception e) 
		{ 
			e.printStackTrace();		
		} 
		
		setTitle("Conectar manzanas");
		this.frame = frame;
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.registros = registros;
		fueEnviado = false;
		initialize();
	}
	
	/**
	 * Devuelve el n�mero de la primer manzana seleccionada por el usuario.
	 */
	public Integer obtenerPrimerManzana() 
	{
		return primerManzana;
	}
	
	/**
	 * Devuelve el n�mero de la segunda manzana seleccionada por el usuario.
	 */
	public Integer obtenerSegundaManzana() 
	{
		return segundaManzana;
	}

	/**
	 * Identifica si fue presionado el bot�n de enviar.
	 * @return true si se presion�, false en caso contrario.
	 */
	public boolean fueEnviado() {
		return fueEnviado;
	}
	
	
	/**
	 * Inicializa los componentes de la ventana.
	 */
	private void initialize() 
	{
		
		//CONFIGURACI�N
		getContentPane().setLayout(null);
		setBounds(515, 263, 310, 173);

		//BOTON ENVIAR
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		
		//COMBO BOX: PRIMER MANZANA
		JComboBox<String> comboBoxManzana1 = crearComboBoxConManzanas(24, btnEnviar);
				
		//COMBO BOX: MANZANA CON LA QUE SE CONECTAR� LA PRIMER MANZANA
		JComboBox<String> comboBoxManzana2 = crearComboBoxConManzanas(68, btnEnviar);
		
		btnEnviar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{ 
				try 
				{
					primerManzana = Integer.parseInt(comboBoxManzana1.getSelectedItem().toString());
					segundaManzana = Integer.parseInt(comboBoxManzana2.getSelectedItem().toString());

					registros.agregarManzanasVecinas(primerManzana, segundaManzana);
					fueEnviado = true;
				}
				catch(Exception ex) 
				{
					JOptionPane.showMessageDialog(frame, ex.getMessage(),"ERROR", 0);
				}
				setVisible(false);
			}
		});
		btnEnviar.setBounds(154, 99, 101, 23);
		getContentPane().add(btnEnviar);
	
		//CREA ETIQUETAS DE INFORMACI�N
		crearEtiquetas();
		
		//BOTON VOLVER
		botonVolver();
	}

	/**
	 * Devuelve un combo box con todas las manzanas del censo.
	 * @param y: corresponde al valor y de la posici�n del combo box.
	 */
	private JComboBox<String> crearComboBoxConManzanas(int y, JButton enviar) 
	{
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		comboBox.setBounds(101, y, 154, 18);
		cargarManzanasComboBox(comboBox, registros.darManzanasRegistradas(), enviar);
		getContentPane().add(comboBox);
		
		return comboBox;
	}

	/**
	 * Guarda los n�meros de manzanas en el combo box comboBox.
	 */
	private void cargarManzanasComboBox (JComboBox<String> comboBox, Set<Integer> manzanas, 
			JButton enviar) 
	{
		if(manzanas.isEmpty()) 
		{
			comboBox.setEnabled(false);
			enviar.setEnabled(false);
		}
		else 
		{
			comboBox.setEnabled(true);
			enviar.setEnabled(true);
			for (Integer numeroManzana : manzanas ) {
				comboBox.addItem(numeroManzana.toString());
			}
		}
	}
	
	/**
	 * Crea el bot�n para cancelar la operaci�n.
	 */
	private void botonVolver() {
		JButton btnVolver = new JButton("Volver");
		btnVolver.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		btnVolver.setBounds(25, 99, 101, 23);
		btnVolver.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				fueEnviado = false;
				setVisible(false);
			}
		});
		getContentPane().add(btnVolver);
	}
	
	/**
	 * Crea las etiquetas correspondiente a la primer manzana
	 * y a la segunda manzana.
	 */
	private void crearEtiquetas() {
		//ETIQUETA PARA ELEGIR PRIMER MANZANA
		JLabel lblEspia1 = new JLabel("Primer manzana");
		lblEspia1.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia1.setBounds(10, 11, 101, 14);
		getContentPane().add(lblEspia1);
		
		//ETIQUETA PARA ELEGIR SEGUNDA MANZANA
		JLabel lblEspia2 = new JLabel("Segunda manzana");
		lblEspia2.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia2.setBounds(10, 44, 114, 21);
		getContentPane().add(lblEspia2);
	}
}