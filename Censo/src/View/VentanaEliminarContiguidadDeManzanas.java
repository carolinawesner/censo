package View;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

import Controller.RegistrosCenso;

public class VentanaEliminarContiguidadDeManzanas extends JDialog {

	private static final long serialVersionUID = 1L;
	private RegistrosCenso registros;
	private boolean fueEnviado;
	private Integer primerManzana;
	private Integer segundaManzana;
	private JComboBox<String> comboBoxManzana1;
	private JComboBox<String> comboBoxManzana2;
	
	/**
	 * Crea el cuadro que permite eliminar la contiguidad entre
	 * dos manzanas seleccionadas.
	 */
	public VentanaEliminarContiguidadDeManzanas(JFrame frame, RegistrosCenso registros) 
	{
		super(frame, true);
		
		try
		{
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} 
		catch(Exception e) 
		{ 
			e.printStackTrace();		
		} 
		
		setTitle("Eliminar contiguidad");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.registros = registros;
		fueEnviado = false;
		initialize();
	}
	
	/**
	 * Devuelve el n�mero de la primer manzana seleccionada por el usuario.
	 */
	public Integer obtenerPrimerManzana() 
	{
		return primerManzana;
	}
	
	/**
	 * Devuelve el n�mero de la segunda manzana seleccionada por el usuario.
	 */
	public Integer obtenerSegundaManzana() 
	{
		return segundaManzana;
	}

	/**
	 * Identifica si fue presionado el bot�n de enviar.
	 * @return true si se presion�, false en caso contrario.
	 */
	public boolean fueEnviado() 
	{
		return fueEnviado;
	}
	
	/**
	 * Inicializaci�n
	 */
	private void initialize() 
	{
		
		getContentPane().setLayout(null);
		setBounds(550, 250, 265, 185);
		
		//COMBO BOX: PRIMER MANZANA
		comboBoxManzana1 = comboBoxPredeterminado(29);
		cargarManzanasComboBox(comboBoxManzana1, registros.darManzanasRegistradas());
		
		//COMBO BOX: MANZANA CON LA QUE SE CONECTA LA PRIMER MANZANA
		comboBoxManzana2 = comboBoxPredeterminado(74);
		
		//BOT�N ENVIAR
		JButton btnEnviar = btnEnviar();
		 
		//accion comboBox1 completa comboBox2 y habilita boton enviar
		comboBoxManzana1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
					
				if(!comboBoxManzana1.getSelectedItem().equals("-")) 
				{

					Integer eleccion = Integer.parseInt(comboBoxManzana1.getSelectedItem().toString());
					
					if(registros.darManzanasContiguasDe(eleccion).size() > 0) 
					{
						Set<Integer> manzanasContiguas = registros.darManzanasContiguasDe(eleccion);
						cargarManzanasComboBox(comboBoxManzana2, manzanasContiguas);
						btnEnviar.setEnabled(true);
					}
					//si la opcion elegida no tiene vecinos
					if(registros.darManzanasContiguasDe(eleccion).size() == 0) 
					{
						comboBoxManzana2.removeAllItems();
						comboBoxManzana2.addItem("-");
						btnEnviar.setEnabled(false);
					}
				}		
			}
		});
		
		btnVolver();
		crearEtiquetasEspias();
	}

	/**
	 * Devuelve el bot�n utilizado para confirmar el env�o de informaci�n seleccionada.
	 */
	private JButton btnEnviar() 
	{
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		btnEnviar.setEnabled(false);
		//accion boton enviar (si est� habilitado)
		btnEnviar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{ 
				//genera la conexion con todos los datos
				primerManzana = Integer.parseInt(comboBoxManzana1.getSelectedItem().toString());
				segundaManzana = Integer.parseInt(comboBoxManzana2.getSelectedItem().toString());
				registros.eliminarContiguidadDeManzanas(primerManzana, segundaManzana);
				fueEnviado = true;
				setVisible(false);	
			}
		});
		btnEnviar.setBounds(147, 103, 89, 23);
		getContentPane().add(btnEnviar);
		
		return btnEnviar;
	}
	
	/**
	 * Crea un bot�n para volver a la pantalla anterior.
	 */
	private void btnVolver() 
	{
		JButton btnVolver = new JButton("Volver");
		btnVolver.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 12));
		btnVolver.setBounds(21, 103, 89, 23);
		btnVolver.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				fueEnviado = false;
				setVisible(false);
			}
		});
		getContentPane().add(btnVolver);
	}

	/**
	 * Carga el conjunto de manzanas al comboBox que recibe por par�metro.
	 * Si el conjunto no tiene elementos no hace nada.
	 * @param comboBox a cargar
	 * @param manzanasContiguas conjunto de String con los n�meros de manzanas.
	 */
	private void cargarManzanasComboBox (JComboBox<String> comboBox, Set<Integer> manzanasContiguas) 
	{
		if (manzanasContiguas.size() > 0) 
		{
			comboBox.removeAllItems();
			for (Integer numeroManzana : manzanasContiguas) 
			{
				comboBox.addItem(numeroManzana.toString());
			}
		}
	}
	
	/**
	 * Crea las etiquetas "Primer manzana" y "Segunda manzana"
	 */
	private void crearEtiquetasEspias() 
	{
		JLabel lblEspia1 = new JLabel("Primer manzana");
		lblEspia1.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia1.setBounds(10, 11, 101, 14);
		getContentPane().add(lblEspia1);
		
		JLabel lblEspia2 = new JLabel("Segunda manzana");
		lblEspia2.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia2.setBounds(10, 49, 114, 21);
		getContentPane().add(lblEspia2);
	}
	
	/**
	 * Crea un combo box y lo coloca en un x predeterminado, pero
	 * con el y pasado por par�metro.
	 */
	private JComboBox<String> comboBoxPredeterminado(int y) 
	{
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		comboBox.setBounds(68, y, 153, 18);
		comboBox.addItem("-");
		getContentPane().add(comboBox);
		return comboBox;
	}
}