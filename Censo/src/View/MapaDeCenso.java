package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;
import org.openstreetmap.gui.jmapviewer.*;

import Controller.RegistrosCenso;
import Model.Manzana;

import java.awt.Toolkit;


public class MapaDeCenso {
	
	private JFrame frmCenso;
	private Fondo fondo = new Fondo("/imagenes/fondoDegradado.jpg");
	private JPanel panelControles;
	private JMapViewer mapa; 
	private Set<MapPolygon> poligonos;
	private RegistrosCenso registros;
	private Map<Integer, MapMarkerCircle> manzanasYpuntos;	
	private Map <String,Set<MapMarkerCircle>> conjManzanasPorCensista;
	private JButton btnCensistas;
	
	/**
	 * Muestra este objeto.
	 */
	public void mostrar() 
	{
		frmCenso.setVisible(true);
	}	
	
	/**
	 * Oculta este objeto.
	 */
	public void ocultar() 
	{
		frmCenso.setVisible(false);
	}

	/**
	 * Crea una ventana que contiene un mapa en el que se mostrar�n
	 * todas las manzanas.
	 * @param d: corresponde a los datos donde se almacenar� la
	 * informaci�n ingresada por el usuario, o, tambi�n, puede ser
	 * correspondiente a datos del radio censal.
	 */
	public MapaDeCenso(RegistrosCenso registros) 
	{
		this.registros = registros;
		manzanasYpuntos = new HashMap<Integer, MapMarkerCircle>();	
		poligonos = new HashSet<MapPolygon>();
		
		try 
		{
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} 
		catch(Exception e) 
		{ 
			e.printStackTrace();		
		} 
		
		initialize();
	}

	/**
	 * Inicializa todos los componentes de esta ventana.
	 */
	private void initialize() 
	{
	
		//FRAME
		frmCenso = new JFrame();
		frmCenso.setIconImage(Toolkit.getDefaultToolkit().getImage(
				MapaDeCenso.class.getResource("/Imagenes/icono censo.jpg")));
		frmCenso.setTitle("Censo 2022");
		frmCenso.setBounds(90, 0, 1200, 700);
		frmCenso.setContentPane(fondo);
		frmCenso.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCenso.setResizable(false);
		frmCenso.getContentPane().setLayout(null);
		
		//MAPA Y PANEL DE MAPA
		JPanel panelMapa = new JPanel();
		panelMapa.setBounds(10, 11, 1164, 588);
		frmCenso.getContentPane().add(panelMapa);
		mapa = new JMapViewer();
		mapa.setLocation(0, 0);
		mapa.setScrollWrapEnabled(true);
		mapa.setDisplayPosition(new Coordinate(-34.521, -58.7008), 15);
		panelMapa.setLayout(null);
		mapa.setSize(2164, 600);
		mapa.setMinimumSize(new Dimension(689, 410));
		panelMapa.add(mapa);
		
		
		//CONTROLES
		panelControles = new JPanel();
		panelControles.setBounds(10, 537, 1164, 113);
		panelControles.setOpaque(false);
		panelControles.setLayout(null);
		frmCenso.getContentPane().add(panelControles);
		
		
		//BOTON DE CENSISTAS
		btnManejoDeCensista();
		
		//ALMACENAMIENTO DE MANZANAS 
		almacenarManzana();
		
		//BOTON ESTABLECER CONTIGUIDAD DE MANZANAS
		btnEstablecerContiguidadDeManzanas();
		
		//BOTON ELIMINAR CONTIGUIDAD DE MANZANAS
		btnEliminarContiguidadDeManzanas();
		
		//BOTON ELIMINAR MANZANA
		btnEliminarManzanaDelMapa();
		
		//BOTON ASIGNAR MANZANAS A CENSISTAS
		btnAsignarManzanas();
		
		//BOTON GUARDAR DATOS
		btnGuardarDatos();
		
		//BOTON VOLVER
		btnVolver();

		//RECUPERACION DE DATOS
		if(registros != null && !registros.isEmpty())
			recuperarDatos();
		else
			RegistrosCenso.establecerManzanaDeInicio(1);

	}
	
	private void btnManejoDeCensista() 
	{
		btnCensistas = HerramientasInterfaz.crearBotonSecundarioPredeterminado("CENSISTAS", 10, 79);
		btnCensistas.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ManejoDeCensistas censistas = new ManejoDeCensistas(frmCenso, registros, conjManzanasPorCensista);
				censistas.mostrar();
				if(censistas.seEliminoCensistaAsignado()) 
				{
					eliminarAsignacion(mapa);
				}
			}
		});
		panelControles.add(btnCensistas);
	}
	
	/**
	 * Elimina la asignaci�n de manzanas.
	 */
	private void eliminarAsignacion(JMapViewer map) 
	{
		if (conjManzanasPorCensista != null) 
		{
			for(String s : conjManzanasPorCensista.keySet()) 
			{
				for(MapMarkerCircle p: conjManzanasPorCensista.get(s))
					map.removeMapMarker(p);
			}
		}
	}
	
	/**
	 * Almacena una manzana en el radio censal.
	 * Esto sucede cuando el usuario hace click en el mapa.
	 */
	private void almacenarManzana() 
	{
		mapa.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1)
				{
					Coordinate markeradd = (Coordinate)
					mapa.getPosition(e.getPoint());	//obtiene las coordenadas del punto donde esta el mouse

					registros.registrarManzana(markeradd.getLon(), markeradd.getLat());
					int numeroDeManzana = registros.obtenerNumeroDeUltimaManzanaCreada();
					
					dibujarManzana(numeroDeManzana, markeradd); 
					
					//elimina asignacion si la hay
					eliminarAsignacion(mapa);
				}
				
			}
		});
	}
	
	private void dibujarManzana(int numeroDeManzana, Coordinate coordenada) 
	{
		MapMarkerCircle puntoVisible = new MapMarkerCircle("   " + numeroDeManzana, coordenada, 0.00030);
		puntoVisible.setColor(Color.BLUE);
		mapa.addMapMarker(puntoVisible);	//crea un circulo con el numero de manzana
		manzanasYpuntos.put(numeroDeManzana, puntoVisible);
	}
	
	/**
	 * Crea un bot�n que establece que dos manzanas son contiguas.
	 * Es decir, que dos manzanas se pueden asignar a un censista ya que
	 * se puede llegar f�cilmente de una a la otra a pie.
	 */
	private void btnEstablecerContiguidadDeManzanas() 
	{
		JButton btnEstablecerContiguidad = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("ESTABLECER CONTIGUIDAD", 240, 79);
		panelControles.add(btnEstablecerContiguidad);
		btnEstablecerContiguidad.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				VentanaConectarManzanas conectar = new VentanaConectarManzanas(frmCenso, registros);
				conectar.setVisible(true);
				if(conectar.fueEnviado()) 
				{
					int numeroPrimerManzana = conectar.obtenerPrimerManzana();
					int numeroSegundaManzana = conectar.obtenerSegundaManzana();
					dibujarContiguidad(numeroPrimerManzana, numeroSegundaManzana);
				}
				//elimina asignacion si la hay
				eliminarAsignacion(mapa);
				
			}
		});
	}
	
	/**
	 * Dibuja en el mapa una linea que representa la contiguidad entre dos manzanas.
	 */
	private void dibujarContiguidad(Integer manzana1, Integer manzana2) 
	{
		ArrayList<Coordinate> coordenadas = new ArrayList<Coordinate>();
		Coordinate coord1 = manzanasYpuntos.get(manzana1).getCoordinate();
		Coordinate coord2 = manzanasYpuntos.get(manzana2).getCoordinate();
		
		coordenadas.add(coord1);
		coordenadas.add(coord2);
		coordenadas.add(coord2);
		
		MapPolygon poligono = new MapPolygonImpl(coordenadas); 
		((MapObjectImpl) poligono).setColor(Color.GREEN); 
		mapa.addMapPolygon(poligono);
		guardarPoligono(poligono);
	}
	
	/**
	 * Guarda un poligono en el conjunto de poligonos.
	 */
	private void guardarPoligono(MapPolygon poligono) 
	{
		poligonos.add(poligono);
	}
	
	/**
	 * Crea un bot�n que elimina la contiguidad de dos manzanas.
	 * Es decir, que establece que dos manzanas no pueden ser 
	 * asignadas a un mismo censista ya que est�n separadas.
	 */
	private void btnEliminarContiguidadDeManzanas() 
	{
		JButton btnEliminarContiguidad = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("ELIMINAR CONTIGUIDAD", 470, 79);
		btnEliminarContiguidad.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				VentanaEliminarContiguidadDeManzanas eliminarContiguidad = new VentanaEliminarContiguidadDeManzanas(frmCenso, registros);
				eliminarContiguidad.setVisible(true);
				
				if(eliminarContiguidad.fueEnviado()) 
				{
					int numeroPrimerManzana = eliminarContiguidad.obtenerPrimerManzana();
					int numeroSegundaManzana = eliminarContiguidad.obtenerSegundaManzana();
					
					eliminarPoligono(numeroPrimerManzana, numeroSegundaManzana);
				}
				//elimina asignacion si la hay
				eliminarAsignacion(mapa);
			}
		});
		panelControles.add(btnEliminarContiguidad);
	}
	
	/**
	 * Elimina todo los pol�gonos de una manzana espec�fica.
	 * @param manzana: corresponde a la manzana cuyos pol�gonos
	 * ser�n eliminados.
	 */
	private void eliminarPoligonosDe(Integer manzana) 
	{
		Coordinate c = manzanasYpuntos.get(manzana).getCoordinate();
		Iterator<MapPolygon> it = poligonos.iterator();
		
		while(it.hasNext()) {
			MapPolygon actual = it.next();
			if(actual.getPoints().contains(c)) {
				it.remove();
				mapa.removeMapPolygon(actual);
			}
		}
	}

	/**
	 * Elimina el pol�gono que tiene a manzana1 y manzana2 en sus extremos.
	 */
	private void eliminarPoligono(Integer manzana1, Integer manzana2) 
	{
		Coordinate c1 = manzanasYpuntos.get(manzana1).getCoordinate();
		Coordinate c2 = manzanasYpuntos.get(manzana2).getCoordinate();
		Iterator<MapPolygon> it = poligonos.iterator();
		
		while(it.hasNext()) 
		{
			MapPolygon actual = it.next();
			if(actual.getPoints().contains(c1) && actual.getPoints().contains(c2)) 
			{
				it.remove();
				mapa.removeMapPolygon(actual);
			}
		}
	}
	
	/**
	 * Elimina una manzana del mapa y su relaci�n con las manzanas vecinas,
	 * asi como los censistas asignados a todo el censo.
	 */
	private void btnEliminarManzanaDelMapa() 
	{
		JButton btnEliminarManzana = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("ELIMINAR MANZANA", 700, 79);
		panelControles.add(btnEliminarManzana);
		btnEliminarManzana.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				Object[] numerosDeManzanas = registros.darManzanasRegistradas().toArray();
				
				Integer numeroDeManzana = (Integer) JOptionPane.showInputDialog(frmCenso, "Manzana a eliminar:", 
						"Eliminar manzana", 0, null, numerosDeManzanas, null);
				
				if(numeroDeManzana != null) 
				{
					eliminarPoligonosDe(numeroDeManzana);
					mapa.removeMapMarker(manzanasYpuntos.get(numeroDeManzana));
					manzanasYpuntos.get(numeroDeManzana);
					registros.eliminarManzana(numeroDeManzana);
					eliminarAsignacion(mapa);
				}
			}
		});
	}
	
	/**
	 * Crea un bot�n que asigna las manzanas a los censistas registrados.
	 */
	private void btnAsignarManzanas() 
	{
		JButton btnAsignarManzanas = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("ASIGNAR CENSISTAS", 930, 79);
		btnAsignarManzanas.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				Set<String> censistasAsignados = new HashSet<String>();

				try 
				{
					censistasAsignados = registros.distribuirCensistasEnRadio();
					etiquetarManzanas(censistasAsignados);
				}
				catch(Exception ex) 
				{
					JOptionPane.showMessageDialog(frmCenso, ex.getMessage(), "ERROR", 0);
				}
				if (!censistasAsignados.isEmpty()) 
				{
					String cantCensistasAsignados = "Se asignaron " + censistasAsignados.size() + 
							" censistas en " + registros.darManzanasRegistradas().size() + " manzanas. "
							+ "�Desea ver esta asignaci�n escrita?";

					int eleccion = JOptionPane.showOptionDialog(frmCenso, cantCensistasAsignados, "INFORMACION",
							JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

					if(eleccion == 0) {
						CuadroDialogo asignacionEscrita = new CuadroDialogo(frmCenso, "Asignaci�n de manzanas", 
								obtenerAsignacionEscrita(censistasAsignados));
						asignacionEscrita.setVisible(true);
					}
						
					
				}
			}
		});
		panelControles.add(btnAsignarManzanas);
	}
	
	/**
	 * Muestra el nombre del censista asignado a la manzana al lado del ID de la manzana.
	 */
	private void etiquetarManzanas(Set<String> censistasAsignados) {
		conjManzanasPorCensista = new HashMap <String,Set<MapMarkerCircle>>();
		for(String c : censistasAsignados) 
		{
			Set<MapMarkerCircle> puntos = new HashSet<MapMarkerCircle>();
			for(Object i : registros.darManzanasDeCensista(c)) 
			{
				MapMarkerCircle punto = manzanasYpuntos.get(i);
				MapMarkerCircle p = new MapMarkerCircle(i + " = "+ c.toUpperCase(), 
						punto.getCoordinate(), 0.00030);
				p.setColor(new Color(136, 7, 250));
				p.setFont(new Font("Ebrima", Font.BOLD, 13));
				p.setBackColor(Color.pink);
				mapa.addMapMarker(p);
				puntos.add(p);
			}
			conjManzanasPorCensista.put(c, puntos);
		}	
	}
	
	/**
	 * Obtiene los nombres de los censistas y sus manzanas asignadas de forma escrita.
	 */
	private String obtenerAsignacionEscrita(Set<String> censistasAsignados) {
		StringBuilder sb = new StringBuilder();
		Stream<String> streamCensistas = censistasAsignados.stream().sorted();
		
		Iterator<String> it = streamCensistas.iterator();
		while(it.hasNext())
		{
			sb.append("Censista ");
			String censista = it.next();
			sb.append(censista);
			sb.append(". Manzanas asignadas: ");
			int cantManzanas = 0;
			for(Object i : registros.darManzanasDeCensista(censista)) 
			{
				cantManzanas++;
				if(cantManzanas != 3) sb.append(i.toString() + ", ");
				else sb.append(i.toString());
			}
			sb.append(". Cantidad de manzanas: " + cantManzanas);
			sb.append("\n");
		}
		return sb.toString();
	}
	
	/**
	 * Crea un bot�n que guarda los datos de este radio censal en un archivo.
	 */
	private void btnGuardarDatos() 
	{
		JButton btnGuardarDatos = new JButton();
		btnGuardarDatos.setBounds(1069, 518, 79, 71);
		mapa.add(btnGuardarDatos);
		btnGuardarDatos.setIcon(new ImageIcon(MapaDeCenso.class.getResource("/imagenes/botonGuardar.png")));
		btnGuardarDatos.setOpaque(false);
		btnGuardarDatos.setContentAreaFilled(false);
		btnGuardarDatos.setBorderPainted(false);
		btnGuardarDatos.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int eleccion = JOptionPane.showConfirmDialog(frmCenso, 
						"Si guarda este radio censal se eliminar� el anterior, �Desea continuar?", 
						"�Desea guardar el radio censal?", 0, 2);
				if(eleccion == 0) 
				{
					boolean seAlmaceno = registros.guardarDatos();
					if(!seAlmaceno)
						JOptionPane.showMessageDialog(frmCenso, 
								"Ocurri� un error. Int�ntelo mas tarde", "ERROR", 0);
					else 
						JOptionPane.showMessageDialog(frmCenso, 
								""
								+ "El radio censal se guard� correctamente", "GUARDADO", 1);
				}	
			}
		});
		
	}

	/**
	 * Crea un bot�n que vuelve a la pantalla principal.
	 */
	private void btnVolver() 
	{
		JButton btnVolver = new JButton();
		btnVolver.setIcon(new ImageIcon(MapaDeCenso.class.getResource("/imagenes/botonVolver.png")));
		btnVolver.setOpaque(false);
		btnVolver.setContentAreaFilled(false);
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				PantallaInicial inicio = new PantallaInicial();
				inicio.mostrar();
				frmCenso.setVisible(false);
			}
		});
		btnVolver.setBounds(1115, 11, 37, 35);
		mapa.add(btnVolver);		
	}
	
	/**
	 * Crea un bot�n que recupera los datos de un radio censal
	 * Esto sucede cuando se llama al constructor con un 
	 * objeto RegistrosCenso que no est� vac�o.
	 * Las manzanas se mostrar�n en el mapa, junto con sus 
	 * manzanas contiguas y el id de cada una.
	 */
	private void recuperarDatos() 
	{
		//modifica el prox id para poder crear nuevas manzanas sin id repetido
		int proxId = 1;
		for(Integer manzana : registros.darManzanasRegistradas()) 
		{
			if(manzana > proxId) 
			{
				proxId = manzana;
			}
			recuperarManzana(manzana);
			
			for(Integer manzanaContigua : registros.darManzanasContiguasDe(manzana)) 
			{
				recuperarManzana(manzanaContigua);
				dibujarContiguidad(manzana, manzanaContigua);
			}
		}
		proxId++;
		Manzana.setProximoID(proxId);
	}

	/**
	 * Recupera de registros los datos correspondientes a la manzana
	 * pasada por par�metro, y la dibuja en el mapa si no est� almacenada
	 * en el map numerosYpuntos.
	 */
	private void recuperarManzana(Integer numeroDeManzana) 
	{
		if(!manzanasYpuntos.containsKey(numeroDeManzana))
		{
			double[] coord = registros.coordenadasDeManzana(numeroDeManzana);
			Coordinate c = new Coordinate(coord[1], coord[0]);
			dibujarManzana(numeroDeManzana, c);
		}
	}
}