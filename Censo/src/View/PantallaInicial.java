package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import Controller.RegistrosCenso;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.Color;

public class PantallaInicial {
	
	private JFrame pantallaInicial;
	private Fondo fondo = new Fondo("/imagenes/fondo.jpg"); 
	private RegistrosCenso registro;

	/**
	 * Muestra este objeto.
	 */
	public void mostrar() 
	{
		pantallaInicial.setVisible(true);
	}	
	
	/**
	 * Oculta este objeto.
	 */
	public void ocultar() 
	{
		pantallaInicial.setVisible(false);
	}
	
	/**
	 *	Ejecuta la aplicaci�n.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					PantallaInicial window = new PantallaInicial();
					window.pantallaInicial.setVisible(true);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Crea la aplicaci�n.
	 */
	public PantallaInicial() 
	{
		try 
		{
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} 
		catch(Exception e) 
		{  
			e.printStackTrace();		
		} 
		
		initialize();
	}

	/**
	 * Inicializa los contenidos de la ventana.
	 */
	private void initialize() 
	{
		pantallaInicial = new JFrame();	
		pantallaInicial.setIconImage(Toolkit.getDefaultToolkit().getImage(
				PantallaInicial.class.getResource("/Imagenes/icono censo.jpg")));
		pantallaInicial.setTitle("Censo 2022");
		pantallaInicial.setContentPane(fondo);
		pantallaInicial.setResizable(false);
		pantallaInicial.setBounds(90, 0, 1200, 700);
		pantallaInicial.setBackground(Color.black);
		pantallaInicial.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pantallaInicial.getContentPane().setLayout(null);
		
		iniciarMapa();
		recuperarUltimoRadioCensal();
		HerramientasInterfaz.crearBarraHerramientas(pantallaInicial);
	}
	
	/**
	 * Crea un bot�n que inicia el radio censal.
	 */
	private void iniciarMapa() 
	{
		JButton btnIniciar = HerramientasInterfaz.crearBotonPantallaInicial("INICIAR NUEVO RADIO CENSAL");
		btnIniciar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{ 
				registro = new RegistrosCenso();
				MapaDeCenso mapa = new MapaDeCenso(registro);
				mapa.mostrar();
				ocultar();
			}
		});
		btnIniciar.setBounds(470, 370, 381, 39);
		pantallaInicial.getContentPane().add(btnIniciar);
	}
	
	/**
	 * Crea un bot�n que recupera el �ltimo radio censal guardado.
	 */
	private void recuperarUltimoRadioCensal() 
	{
		JButton btnRecuperaRadio = HerramientasInterfaz.crearBotonPantallaInicial("RECUPERAR ULTIMO RADIO CENSAL");
		btnRecuperaRadio.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {
				try 
				{
					registro = RegistrosCenso.recuperarDatos();
					MapaDeCenso mapa = new MapaDeCenso(registro);
					mapa.mostrar();
					ocultar();
				} 
				catch (Exception ex) 
				{
					JOptionPane.showMessageDialog(pantallaInicial, 
							"Ocurri� un error. Int�ntelo mas tarde", "ERROR", 0);
				}
			}
		});
		btnRecuperaRadio.setBounds(470, 430, 381, 39);
		pantallaInicial.getContentPane().add(btnRecuperaRadio);
	}
}