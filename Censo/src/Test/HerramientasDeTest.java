package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Model.Censista;
import Model.Censo;
import Model.Grafo;
import Model.Manzana;
/**
 * Esta clase provee herramientas para facilitar el testing del proyecto.
 * @author Caro, Xime
 */
public class HerramientasDeTest {
	
	//Manzanas para registrar
	private static Manzana m1 = new Manzana();
	private static Manzana m2 = new Manzana();
	private static Manzana m3 = new Manzana();
	private static Manzana m4 = new Manzana();
	private static Manzana m5 = new Manzana();
	private static Manzana m6 = new Manzana();
	
	//GRAFO
/*    A--B
 *    |  | >D
 *    E--C
 * 
 */
	/**
	 * Devuelve un grafo conexo.
	 * @return
	 */
	public static Grafo<String> grafoConexo()
	{
		Grafo<String> grafo = new Grafo<String>();
		grafo.agregarArista("A", "B");
		grafo.agregarArista("A", "E");
		grafo.agregarArista("B", "C");
		grafo.agregarArista("B", "D");
		grafo.agregarArista("E", "C");
		grafo.agregarArista("C", "D");
			
		return grafo;
	}
	
	/**
	 * Devuelve un grafo no conexo.
	 * @return
	 */
	public static Grafo<String> grafoNoConexo()
	{
		Grafo<String> grafo = new Grafo<String>();
		grafo.agregarArista("A", "B");
		grafo.agregarArista("A", "E");
		grafo.agregarArista("B", "C");
		grafo.agregarArista("B", "D");
		grafo.agregarArista("E", "C");
		grafo.agregarArista("C", "D");
		grafo.agregarVertice("F");
			
		return grafo;
	}
	
	/**
	 * Evalua si un arreglo de cadenas tiene los mismos elementos que una lista de cadenas.
	 * @param esperado Arreglo de cadenas.
	 * @param obtenido Lista de cadenas.
	 */
	public static void iguales(String[] esperado, List<String> obtenido)
	{
		assertEquals(esperado.length, obtenido.size());
		for(int i=0; i<esperado.length; ++i)
			assertTrue( obtenido.contains(esperado[i]) );
	}
	
	//CENSO
	/**
	 * Devuelve un censo vacio.
	 * @return Censo.
	 */
	public static Censo censoVacio() 
	{
		Censo c = new Censo();
		return c;
	}
	
	/*	1--2--3
	 *  |  |  |
	 * 	4--5--6
	 */
	
	/**
	 * Devuelve un censo con todas sus manzanas relacionadas.
	 * @return Censo.
	 */
	public static Censo censoConManzanasContiguas() 
	{
		Censo c = new Censo();

		c.agregarManzana(m1);
		c.agregarManzana(m2);
		c.agregarManzana(m3);
		c.agregarManzana(m4);
		c.agregarManzana(m5);
		c.agregarManzana(m6);

		c.registrarManzanasVecinas(m1, m2);
		c.registrarManzanasVecinas(m2, m3);
		c.registrarManzanasVecinas(m1, m4);
		c.registrarManzanasVecinas(m2, m5);
		c.registrarManzanasVecinas(m3, m6);
		c.registrarManzanasVecinas(m4, m5);
		c.registrarManzanasVecinas(m5, m6);
		
		return c;
	}
	
	/**
	 * Devuelve un censo sin manzanas relacionadas.
	 * @return Censo.
	 */
	public static Censo censoConManzanasNoContiguas() 
	{
		Censo c = new Censo();
		
		c.agregarManzana(m1);
		c.agregarManzana(m2);
		c.agregarManzana(m3);
		c.agregarManzana(m4);
		c.agregarManzana(m5);
		return c;
	}
	
	/**
	 * Devuelve un conjunto de cinco censistas.
	 * @return Set de Censista con cinco elementos.
	 */
	public static Set<Censista> darUnConjDe5Censista()
	{
		Set<Censista> censistas = new HashSet<Censista>();
		censistas.add(new Censista("C1"));
		censistas.add(new Censista("C2"));
		censistas.add(new Censista("C3"));
		censistas.add(new Censista("C4"));
		censistas.add(new Censista("C5"));
		return censistas;
	}
	
	/**
	 * Registra en un censo a un conjunto de censistas.
	 * @param c Censo.
	 * @param censistas Set de Censista.
	 */
	public static void registrarConjuntoCensistas(Censo c, Set<Censista> censistas) 
	{
		for(Censista cen: censistas)
			c.registrarCensista(cen);
	}
	
	/**
	 * Devuelve una manzana registrada en el censo.
	 * @return Manzana
	 */
	public static Manzana darUnaManzana() 
	{
		return m6;
	}
	
	/**
	 * Devuelve un arreglo de dos manzanas vecinas.
	 * @return Arreglo de manzanas.
	 */
	public static Manzana[] darArregloDosManzanasContiguas() 
	{
		Manzana [] ret = {m1, m2};
		return ret;
	}
	
	/**
	 * Evalua si un conjunto de censistas pertenece a las asignaciones del censo.
	 * @param censistas
	 * @param censo
	 * @return true o false.
	 */
	public static boolean evaluarManzanasAsignadas(Set<Censista> censistas, Censo censo) 
	{
		boolean ret = true;
		for(Censista c : censistas) {
			for(Manzana m: c.darManzanas()) {
				if(!censo.esManzanaRegistrada(m))
					ret = false;
			}
		}
		return ret;
	}
}
