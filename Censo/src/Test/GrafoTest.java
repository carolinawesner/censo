package Test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import Model.Grafo;

/**
 * Esta clase implementa test unitarios sobre la clase Grafo.
 * @author Caro, Xime.
 */

public class GrafoTest {

	Grafo<String> g = new Grafo<String>();
	
	//VERTICES
	@Test (expected = IllegalArgumentException.class)
	public void agregarVerticeExistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.agregarVertice("A");
	}
	
	@Test 
	public void agregarVerticeInexistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.agregarVertice("G");
		
		assertTrue(g.existeVertice("G"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarVerticeInexistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.eliminarVertice("G");
	}
	
	@Test
	public void eliminarVerticeExistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.eliminarVertice("A");
		
		assertFalse(g.existeVertice("A"));
		assertFalse(g.vecinos("B").contains("A"));
	}
	
	@Test
	public void darVerticesTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		LinkedList<String> vertices = new LinkedList<String>();
		vertices.addAll(g.darVertices());
		HerramientasDeTest.iguales(new String[] {"A", "B", "C", "D", "E"} , vertices);
	}
	
	@Test
	public void darVerticesGrafoVacioTest() 
	{
		assertTrue(g.darVertices().isEmpty());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void darUnVerticesGrafoVacioTest() 
	{
		g.darUnVertice().isEmpty();
	}
	
	@Test
	public void darUnVerticeTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		assertTrue(g.existeVertice(g.darUnVertice()));
	}
	
	
	//TAMA�O
	@Test
	public void tamanioTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		
		assertEquals(5,g.tamanio()); 
	}
	
	//ARISTAS
	@Test (expected = IllegalArgumentException.class)
	public void agregarAristaExistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.agregarArista("A", "B");
	}
	
	@Test
	public void agregarAristaAmbosVerticesInexitentesTest() 
	{
		g = HerramientasDeTest.grafoNoConexo();
		g.agregarArista("G", "H");
		
		assertTrue(g.existeVertice("G"));
		assertTrue(g.existeVertice("H"));
		assertTrue(g.existeArista("G", "H"));
	}
	
	@Test
	public void agregarAristaUnVerticeInexitentesTest() 
	{
		g = HerramientasDeTest.grafoNoConexo();
		g.agregarArista("A", "H");
		
		assertTrue(g.existeVertice("H"));
		assertTrue(g.existeArista("A", "H"));
	}
	
	@Test
	public void agregarAristaVerticesExitentesTest() 
	{
		g = HerramientasDeTest.grafoNoConexo();
		g.agregarArista("D", "F");
		
		assertTrue(g.existeArista("D", "F"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaVerticesIgualesTest() 
	{
		g = HerramientasDeTest.grafoNoConexo();
		g.agregarArista("A", "A");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarAristaInexistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.eliminarArista("W", "Z");;
	}
	
	@Test
	public void eliminarAristaExistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.eliminarArista("A", "B");
		
		assertFalse(g.existeArista("A", "B"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void existeAristaInexistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.existeArista("W", "Z");
	}
	
	@Test 
	public void existeAristaExistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		assertTrue(g.existeArista("A", "B"));
	}
	
	//VECINOS
	@Test (expected = IllegalArgumentException.class)
	public void vecinosVerticeInexistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		g.vecinos("W");
		
	}
	
	@Test 
	public void vecinosVerticeExistenteTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		LinkedList<String> vecinos = new LinkedList<String>();
		vecinos.addAll(g.vecinos("A"));
		HerramientasDeTest.iguales(new String[] {"B", "E"} , vecinos);
	}
	
	//EQUALS
	@Test 
	public void grafosIgualesTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		Grafo<String> g2 = new Grafo<String>();
		g2.agregarArista("A", "B");
		g2.agregarArista("A", "E");
		g2.agregarArista("B", "C");
		g2.agregarArista("B", "D");
		g2.agregarArista("E", "C");
		g2.agregarArista("C", "D");
		
		assertTrue(g.equals(g2));
	}
	
	@Test 
	public void grafosDistintosTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		Grafo<String> g2 = HerramientasDeTest.grafoNoConexo();
		
		assertFalse(g.equals(g2));
	}
	
	@Test 
	public void grafosConVerticesIgualesDistintaAristasTest() 
	{
		g = HerramientasDeTest.grafoConexo();
		Grafo<String> g2 = new Grafo<String>();
		g2.agregarArista("A", "C");
		g2.agregarArista("A", "D");
		g2.agregarArista("C", "E");
		g2.agregarArista("C", "B");
		g2.agregarArista("D", "B");
		g2.agregarArista("B", "E");
		
		assertFalse(g.equals(g2));
	}
}