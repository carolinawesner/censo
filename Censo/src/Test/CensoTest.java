package Test;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import Model.Censista;
import Model.Censo;
import Model.Manzana;
/**
 * Esta clase implementa un conjunto de test unitarios para la clase Censo.
 * @author Caro, Xime
 */

public class CensoTest {

	private Censo censo;
	
	//ASIGNACION
	@Test 
	public void asignarCensistasManzanasContiguasTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Set<Censista> censistasDisponibles = HerramientasDeTest.darUnConjDe5Censista();
		HerramientasDeTest.registrarConjuntoCensistas(censo, censistasDisponibles);
		Set<Censista> asignados = censo.asignarManzanas();
		
		assertEquals(2, asignados.size());
		assertTrue(censistasDisponibles.containsAll(asignados));
		assertTrue(censistasDisponibles.containsAll(asignados));
		assertTrue(HerramientasDeTest.evaluarManzanasAsignadas(asignados, censo));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void asignarCensistasManzanasNoContiguasTest() 
	{
		censo = HerramientasDeTest.censoConManzanasNoContiguas();
		HerramientasDeTest.registrarConjuntoCensistas(censo,HerramientasDeTest.darUnConjDe5Censista());
		censo.asignarManzanas();
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void asignarSinRegistrarCensistasTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		censo.asignarManzanas();
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void asignarConCensistasInsuficientesTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Censista c1 = new Censista("c1");
		censo.registrarCensista(c1);
		censo.asignarManzanas();
	}
	
	//MANZANAS
	@Test 
	public void agregarManzanaTest() 
	{
		censo = HerramientasDeTest.censoVacio();
		Manzana m = HerramientasDeTest.darUnaManzana();
		censo.agregarManzana(m);
		
		assertTrue(censo.esManzanaRegistrada(m));
	}
	
	@Test 
	public void eliminarManzanaTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Manzana m = HerramientasDeTest.darUnaManzana();
		censo.eliminarManzana(m);
		
		assertFalse(censo.esManzanaRegistrada(m));
	}
	
	@Test 
	public void registrarManzanasVecinasInexistentesTest() 
	{
		censo = HerramientasDeTest.censoVacio();
		Manzana m7 = new Manzana();
		Manzana m8 = new Manzana();
		censo.agregarManzana(m7);
		censo.agregarManzana(m8);
		censo.registrarManzanasVecinas(m7, m8);
		
		assertTrue(censo.darVecinosDe(m7).contains(m8) 
				&& censo.darVecinosDe(m8).contains(m7));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void registrarManzanasVecinasExistentesTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Manzana m1 = HerramientasDeTest.darArregloDosManzanasContiguas()[0];
		Manzana m2 = HerramientasDeTest.darArregloDosManzanasContiguas()[1];
		
		censo.registrarManzanasVecinas(m1, m2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarManzanasVecinasInexistentesTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Manzana m9 = new Manzana();
		Manzana m = HerramientasDeTest.darUnaManzana();
		
		censo.registrarManzanasVecinas(m9, m);
	}
	
	@Test 
	public void eliminarManzanasVecinasExistentesTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Manzana m1 = HerramientasDeTest.darArregloDosManzanasContiguas()[0];
		Manzana m2 = HerramientasDeTest.darArregloDosManzanasContiguas()[1];
		
		censo.eliminarRegistroManzanasVecinas(m1, m2);
		
		assertFalse(censo.darVecinosDe(m1).contains(m2) 
				&& censo.darVecinosDe(m2).contains(m1));
	}
	
	//CENSISTAS
	@Test 
	public void registrarCensistaInexistenteTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Censista c = new Censista("c");
		censo.registrarCensista(c);
		
		assertTrue(censo.esCensistaRegistrado(c));
	}
	
	@Test 
	public void eliminarCensistaExistenteTest()
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Censista c = new Censista("c");
		censo.registrarCensista(c);
		censo.eliminarCensista(c);
		
		assertFalse(censo.esCensistaRegistrado(c));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarCensistaInexistenteTest() 
	{
		censo = HerramientasDeTest.censoConManzanasContiguas();
		Censista c = new Censista("c");
		censo.eliminarCensista(c);
	}	
}
