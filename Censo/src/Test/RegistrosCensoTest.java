package Test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import Controller.RegistrosCenso;
/**
 * Esta clase implementa tests unitarios sobre la clase RegistrosCenso
 * @author Caro, Xime
 */

public class RegistrosCensoTest {
	RegistrosCenso r;
	
	//CENSISTAS
	@Test(expected = IllegalArgumentException.class)
	public void registrarCensistaNombreEnBlancoTest() 
	{
		r = new RegistrosCenso();
		r.registrarCensista(" ");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void registrarCensistaNombreEnVacioTest() 
	{
		r = new RegistrosCenso();
		r.registrarCensista("");
	}
	
	
	@Test
	public void registrarCensistaInexistenteTest() 
	{
		r = new RegistrosCenso();
		r.registrarCensista("c1");

		assertTrue(r.darNombresDeCensistas().contains("c1"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void registrarCensistaExistenteTest() 
	{
		r = new RegistrosCenso();
		r.registrarCensista("c1");
		r.registrarCensista("c1");
	}
	
	@Test
	public void eliminarCensistaExistenteTest() 
	{
		r = new RegistrosCenso();
		r.registrarCensista("c1");
		r.eliminarCensistaRegistrado("c1");
		
		assertFalse(r.darNombresDeCensistas().contains("c1"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarCensistaInexistenteTest() 
	{
		r = new RegistrosCenso();
		r.eliminarCensistaRegistrado("c1");
	}
	
	@Test
	public void darNombresDeCensistasTest() 
	{
		r = new RegistrosCenso();
		r.registrarCensista("c1");
		r.registrarCensista("c2");

		assertTrue(r.darNombresDeCensistas().contains("c1") 
				&& r.darNombresDeCensistas().contains("c2"));
	}
	
	//MANZANAS
	@Test
	public void registrarManzanaTest() 
	{
		r = new RegistrosCenso();
		r.registrarManzana(0.1, 0.2);
		int manzana = r.obtenerNumeroDeUltimaManzanaCreada();
		
		assertTrue(r.darManzanasRegistradas().contains(manzana));
	}
	
	@Test
	public void eliminarManzanaTest() 
	{
		r = new RegistrosCenso();
		r.registrarManzana(0.1, 0.2);
		int manzana = r.obtenerNumeroDeUltimaManzanaCreada();
		r.eliminarManzana(manzana);
		
		assertFalse(r.darManzanasRegistradas().contains(manzana));
	}
	
	@Test
	public void establecerManzanaDeInicioTest() 
	{
		r = new RegistrosCenso();
		r.registrarManzana(0.1, 0.2);
		int primerManzana = r.obtenerNumeroDeUltimaManzanaCreada();
		RegistrosCenso.establecerManzanaDeInicio(100);
		r.registrarManzana(0.1, 0.2);
		
		Set<Integer> expected = new HashSet<Integer>();
		expected.add(primerManzana);
		expected.add(100);
		
		assertEquals(expected, r.darManzanasRegistradas());
	}
	
	@Test
	public void coordenadasManzanaTest() 
	{
		r = new RegistrosCenso();
		r.registrarManzana(0.1, 0.2);
		int manzana = r.obtenerNumeroDeUltimaManzanaCreada();
		
		assertTrue(0.1 == r.coordenadasDeManzana(manzana)[0]);
		assertTrue(0.2 ==r.coordenadasDeManzana(manzana)[1]);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void coordenadasManzanaInexistenteTest()
	{
		r = new RegistrosCenso();
		r.coordenadasDeManzana(10000);
	}
	
	@Test
	public void agregarManzanasVecinasTest() 
	{
		r = new RegistrosCenso();
		
		r.registrarManzana(0.1, 0.2);
		int manzana1 = r.obtenerNumeroDeUltimaManzanaCreada();
		
		r.registrarManzana(0.3, 0.4);
		int manzana2 = r.obtenerNumeroDeUltimaManzanaCreada();

		r.agregarManzanasVecinas(manzana1, manzana2);
		assertTrue(r.sonManzanasContiguas(manzana1, manzana2));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarManzanasVecinasAmbasInexistentesTest() 
	{
		r = new RegistrosCenso();
		r.agregarManzanasVecinas(15, 16);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarManzanasVecinasUnaInexistenteTest() 
	{
		r = new RegistrosCenso();
		r.registrarManzana(0.1, 0.2);
		r.agregarManzanasVecinas(1, 17);
	}
	
	@Test
	public void sonManzanasContiguasTest() 
	{
		r = new RegistrosCenso();
		
		r.registrarManzana(0.1, 0.2);
		int manzana1 = r.obtenerNumeroDeUltimaManzanaCreada();
		r.registrarManzana(0.1, 0.2);
		int manzana2 = r.obtenerNumeroDeUltimaManzanaCreada();
		
		r.agregarManzanasVecinas(manzana1, manzana2);
		assertTrue(r.sonManzanasContiguas(manzana1, manzana2));
	}
	
	@Test
	public void darManzanasContiguasDeTest() 
	{
		r = new RegistrosCenso();
		
		r.registrarManzana(0.1, 0.2);
		int manzana1 = r.obtenerNumeroDeUltimaManzanaCreada();
		r.registrarManzana(0.1, 0.2);
		int manzana2 = r.obtenerNumeroDeUltimaManzanaCreada();
		
		r.agregarManzanasVecinas(manzana1, manzana2);
		
		assertTrue(r.darManzanasContiguasDe(manzana1).size() == 1 &&  
				r.darManzanasContiguasDe(manzana1).contains(manzana2));
	}
	
	@Test
	public void eliminarContiguidadDeManzanasTest() 
	{
		r = new RegistrosCenso();
		
		r.registrarManzana(0.1, 0.2);
		int manzana1 = r.obtenerNumeroDeUltimaManzanaCreada();
		r.registrarManzana(0.1, 0.2);
		int manzana2 = r.obtenerNumeroDeUltimaManzanaCreada();
		
		r.agregarManzanasVecinas(manzana1, manzana2);
		r.eliminarContiguidadDeManzanas(manzana1, manzana2);

		assertFalse(r.sonManzanasContiguas(manzana1, manzana2));
	}
	
	@Test
	public void isEmptyHappyPathTest()
	{
		r = new RegistrosCenso();
		assertTrue(r.isEmpty());
	}
	
	@Test
	public void noIsEmptyTest()
	{
		r = new RegistrosCenso();
		r.registrarManzana(0.1, 0.2);
		assertFalse(r.isEmpty());
	}
}
