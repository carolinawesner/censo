package Test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import Model.BFS;
import Model.Grafo;

/**
 * Esta clase implementa tests unitarios sobre la clase BFS.
 * @author Caro, Xime.
 */

public class BFSTest {

	//GRAFO NULO
	@Test
	public void grafoNuloTest() 
	{
		Grafo<String> g = new Grafo<String>();
		@SuppressWarnings("unused")
		BFS<String> bfs = new BFS<String>(g);
	}
	
	//CONEXO
	@Test
	public void esConexoGrafoConexoTest() 
	{
		BFS<String> bfs = new BFS<String>(HerramientasDeTest.grafoConexo());
		assertTrue(bfs.esConexo());
	}

	@Test
	public void esConexoGrafoNoConexoTest() 
	{
		BFS<String> bfs = new BFS<String>(HerramientasDeTest.grafoNoConexo());
		assertFalse(bfs.esConexo());	
	}
	
	//ALCANZABLES
	@Test
	public void alcanzablesGrafoConexoTest() 
	{
		BFS<String> bfs = new BFS<String>(HerramientasDeTest.grafoConexo());
		LinkedList<String> alcanzables = bfs.alcanzables(HerramientasDeTest.grafoConexo(),
				HerramientasDeTest.grafoConexo().darUnVertice());
		HerramientasDeTest.iguales(new String [] {"A", "B", "C", "D", "E"}, alcanzables);	
	}
	
	@Test
	public void alcanzablesGrafoNoConexoTest() 
	{
		BFS<String> bfs = new BFS<String>(HerramientasDeTest.grafoNoConexo());
		LinkedList<String> alcanzables = bfs.alcanzables(HerramientasDeTest.grafoNoConexo(),
				HerramientasDeTest.grafoNoConexo().darUnVertice());
		HerramientasDeTest.iguales(new String [] {"A", "B", "C", "D", "E"}, alcanzables);
		assertFalse(alcanzables.contains("F"));
	}	
}
