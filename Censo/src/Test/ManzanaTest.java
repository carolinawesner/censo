package Test;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import Model.Manzana;

/**
* Esta clase implementa tests unitarios sobre la clase Manzana.
* @author Caro, Xime.
*/

@FixMethodOrder(MethodSorters.NAME_ASCENDING) 
public class ManzanaTest {
	Manzana m1;
	
	@Test
	public void a1_darIdTest() 
	{
		m1 = new Manzana();
		int id = m1.darId();
		assertEquals(1, id);
	}
	
	@Test
	public void a2_darProxIdTest() 
	{
		assertEquals(2, Manzana.darProximoID());
	}
	
	@Test
	public void a3_coordenadasTest() 
	{
		m1 = new Manzana();
		double[] coor = {0.1, 0.2};
		m1.setCoord(coor);
		
		assertTrue(m1.getCoord()[0] == 0.1 && m1.getCoord()[1] == 0.2);
	}
	
	@Test
	public void a4_manzanasIgualesTest() 
	{
		m1 = new Manzana();
		double[] coor = {0.1, 0.2};
		m1.setCoord(coor);
		Manzana m2 = m1;
		
		assertTrue(m1.equals(m2));
	}

	@Test
	public void a5_manzanasDiferentesTest() 
	{
		m1 = new Manzana();
		double[] coor = {0.1, 0.2};
		m1.setCoord(coor);
		Manzana m2 = new Manzana();
		m2.setCoord(coor);
		
		assertFalse(m1.equals(m2));
	}
	
	@Test
	public void a6_equalsTest() {
		m1 = new Manzana();
		Manzana m2 = new Manzana();
		double[] coor = {0.1, 0.2};
		m1.setCoord(coor);
		m2.setCoord(coor);
		
		assertFalse(m1.equals(m2));
	}
	
	@Test
	public void a7_equalsHappyPathTest()
	{
		Manzana.setProximoID(1);
		m1 = new Manzana();
		Manzana.setProximoID(1);
		Manzana m2 = new Manzana();
		double[] coor = {0.1, 0.2};
		m1.setCoord(coor);
		m2.setCoord(coor);
		
		assertTrue(m1.equals(m2));
	}
	
	@Test
	public void a8_equalsMismoObjeto()
	{
		m1 = new Manzana();
		double[] coor = {0.1, 0.2};
		m1.setCoord(coor);
		
		Manzana m2 = m1;
		
		assertTrue(m1.equals(m2));
	}
	
	@Test
	public void a9_equalsManzanasCoordenadasDistintas() 
	{
		m1 = new Manzana();
		double[] coorM1 = {0.1, 0.2};
		m1.setCoord(coorM1);
		
		Manzana m2 = new Manzana();
		double[] coorM2 = {0.1, 0.1};
		m2.setCoord(coorM2);
		
		assertFalse(m1.equals(m2));
	}
	
	
}
