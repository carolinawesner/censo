package Test;

import static org.junit.Assert.*;
import org.junit.Test;

import Model.Censista;
import Model.Manzana;
/**
 * Esta clase implementa tests unitarios sobre la clase Censista.
 * @author Caro, Xime.
 */

public class CensistaTest {

	@Test (expected = IllegalArgumentException.class)
	public void crearCensistaNombreVacioTest() 
	{
		@SuppressWarnings("unused")
		Censista c = new Censista("");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void crearCensistaNombreEnBlancoTest() 
	{
		@SuppressWarnings("unused")
		Censista c = new Censista(" ");
	}
	
	@Test 
	public void getNombreTest() 
	{
		Censista c = new Censista("C1");
		
		assertEquals("C1", c.getNombre());
	}
	
	@Test 
	public void asignarManzanaTest() 
	{
		Censista c = new Censista("C1");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c.asignarManzana(m);
		
		assertTrue(c.darManzanas().contains(m));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void asignarManzanarYaAsignadaTest() 
	{
		Censista c = new Censista("C1");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c.asignarManzana(m);
		c.asignarManzana(m);
	}
	
	@Test 
	public void cantidadManzanasAsignadasTest() 
	{
		Censista c = new Censista("C1");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c.asignarManzana(m);
		
		assertEquals(1, c.cantidadDeManzanasAsignadas());
	}
	
	@Test 
	public void eliminarManzanasAsignadasTest() 
	{
		Censista c = new Censista("C1");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c.asignarManzana(m);
		c.eliminarManzanaAsignadas();
		
		assertEquals(0, c.cantidadDeManzanasAsignadas());
	}
	
	@Test 
	public void eliminarManzanaRegistradaTest() 
	{
		Censista c = new Censista("C1");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c.asignarManzana(m);
		c.eliminarManzana(m);
		
		assertFalse(c.darManzanas().contains(m));
	}
	
	@Test 
	public void darManzanasRegistradasTest() 
	{
		Censista c = new Censista("C1");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c.asignarManzana(m);

		assertEquals(1, c.darManzanas().size());
		assertTrue(c.darManzanas().contains(m));
	}
	
	@Test 
	public void censistasIgualesTest() 
	{
		Censista c1 = new Censista("C");
		Censista c2 = new Censista("C");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c1.asignarManzana(m);
		c2.asignarManzana(m);

		assertTrue(c1.equals(c2));
	}
	
	@Test 
	public void censistasIgualNombreDistintasManzanasTest() 
	{
		Censista c1 = new Censista("C");
		Censista c2 = new Censista("C");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c1.asignarManzana(m);
		
		assertFalse(c1.equals(c2));
	}
	
	@Test 
	public void censistasDistintoNombreIgualManzanasTest() 
	{
		Censista c1 = new Censista("C1");
		Censista c2 = new Censista("C2");
		Manzana m = HerramientasDeTest.darUnaManzana();
		c1.asignarManzana(m);
		c2.asignarManzana(m);
		
		assertFalse(c1.equals(c2));
	}
	
	
	

}
