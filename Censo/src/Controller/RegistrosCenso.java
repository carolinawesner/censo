package Controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import Model.Censista;
import Model.Censo;
import Model.Manzana;

/**
 * Esta clase permite al usuario interactuar directamente con ella.
 * Almacena la informaci�n de los censistas y las manzanas del radio censal 
 * ingresados por el usuario, y permite modificar toda la informaci�n respecto a ellos.
 * @author Caro, Xime
 */

public class RegistrosCenso implements Serializable {

	private static final long serialVersionUID = 1L;
	private Censo _censo;                    //Censo armado a partir de los datos ingresados.
	private Map<String,Censista> _censistas; //Diccionario que relaciona el nombre del censista 
											 //con el objeto censista correspondiente.
	private Map<Integer, Manzana> _manzanas; //Diccionario que relaciona el id de la manzana
											 //con el objeto manzana correspondiente.

	public RegistrosCenso() 
	{
		_censo = new Censo();
		_censistas= new HashMap<String, Censista>();
		_manzanas = new HashMap<Integer, Manzana>();

	}
	
	/**
	 * Registra el cencista que corresponde al nombre pasado por par�metro.
	 * @param nombre String que corresponde al nombre a registrar.
	 * @throws IllegalArgumentException si el nombre ya fue registrado.
	 */
	public void registrarCensista(String nombre) 
	{
		if(_censistas.keySet().contains(nombre))
			throw new IllegalArgumentException("El nombre ingresado ya fue registrado");
		if(nombre.isBlank() || nombre.isEmpty())
			throw new IllegalArgumentException("El nombre ingresado no es v�lido");
		
		else
		{
			Censista c = new Censista(nombre);
			_censistas.put(nombre, c);
			_censo.registrarCensista(c);
		}
	}
	
	/**
	 * Elimina el censita registrado correspondiente al nombre pasado por parametro
	 * @param nombre String que corresponde al nombre a eliminar.
	 * @throws IllegalArgumentException si el nombre no est� registrado.
	 */
	public void eliminarCensistaRegistrado(String nombre) {
		if(!_censistas.keySet().contains(nombre))
			throw new IllegalArgumentException("El nombre ingresado no fue registrado");
		else
		{
			Censista c = _censistas.get(nombre);
			_censistas.remove(nombre);
			_censo.eliminarCensista(c);
		}
	}
	
	/**
	 * Establece el n�mero a partir del cual se crean las manzanas.
	 * @param numeroDeManzana corresponde al n�mero de la primer manzana.
	 */
	public static void establecerManzanaDeInicio(int numeroDeManzana)
	{
		Manzana.setProximoID(numeroDeManzana);
	}
	
	/**
	 * Registra una nueva manzana con un id �nico y con las 
	 * coordenadas establecidas por par�metro.
	 */
	public void registrarManzana(double x, double y) {
		Manzana m = new Manzana();
		double[] coord = {x, y};
		m.setCoord(coord);
		_manzanas.put(m.darId(), m);
		_censo.agregarManzana(m);
	}
	
	/**
	 * Devuelve el id de la �ltima manzana registrada.
	 * @return int que corresponde al id de esta manzana.
	 */
	public int obtenerNumeroDeUltimaManzanaCreada() 
	{
		return Manzana.darProximoID() - 1;
	}
	
	/**
	 * Elimina la manzana con el id especificado.
	 * @param id n�mero de identificaci�n de la manzana.
	 */
	public void eliminarManzana(Integer id) {
		Manzana m = _manzanas.get(id);
		_censo.eliminarManzana(m);
		_manzanas.remove(id);
	}
	
	/**
	 * Devuelve las coordenadas de la manzana especificada por par�metro.
	 * @returns double[] en el que en la posicion 0 esta la coordenada X,
	 * y en la posicion 1 esta la coordenada Y.
	 * @throws IllegalArgumentException si la manzana no esta registrada.
	 */
	public double[] coordenadasDeManzana(int numeroDeManzana) 
	{
		if(!_manzanas.containsKey(numeroDeManzana))
			throw new IllegalArgumentException("La manzana no esta registrada.");
		return _manzanas.get(numeroDeManzana).getCoord();
	}
	
	/**
	 * Registra manzanas contiguas.
	 * @param m1 primer manzana a relacionar. 
	 * @param m2 segunda manzana a relacionar. 
	 */
	public void agregarManzanasVecinas(Integer m1, Integer m2)
	{
		verificarId(m1,m2);
		_censo.registrarManzanasVecinas(_manzanas.get(m1), _manzanas.get(m2));
	}
	
	/**
	 * Verifica que los id de las manzanas sean v�lidos.
	 * @param i primer n�mero.
	 * @param j segundo n�mero.
	 * @throws IllegalArgumentException si las manzanas son iguales o alguna no est�
	 * registrada.
	 */
	private void verificarId(Integer i, Integer j) 
	{
		if(i == j)
			throw new IllegalArgumentException("Las manzanas son iguales");
		else if(!_manzanas.containsKey(i))
			throw new IllegalArgumentException("La manzana " + i + " no est� registrada");
		else if(!_manzanas.containsKey(j))
			throw new IllegalArgumentException("La manzana " + j + " no est� registrada");
	}

	/**
	 * Devuelve el conjunto de nombres de los censistas registrados.
	 * @return Set de String.
	 */
	public Set<String> darNombresDeCensistas(){
		return this._censistas.keySet();
	}

	/**
	 * Eval�a si las manzanas estan contiguas.
	 * @param primerManzana
	 * @param segundaManzana
	 * @return true si son contiguas y false en caso contrario.
	 */
	public boolean sonManzanasContiguas(Integer primerManzana, Integer segundaManzana) 
	{
		return _censo.darVecinosDe(_manzanas.get(primerManzana)).contains(_manzanas.get(segundaManzana))
				&& _censo.darVecinosDe(_manzanas.get(segundaManzana)).contains(_manzanas.get(primerManzana));
	}

	/**
	 * Devuelve el conjunto de manzanas registradas.
	 * @return Set de manzanas.
	 */
	public Set<Integer> darManzanasRegistradas()
	{
		return this._manzanas.keySet();
	}

	/**
	 * Devuelve el conjunto de id de manzanas contiguas de la manzana correspondiente 
	 * al id pasado por par�metro.
	 * @param eleccion id de la manzana a obtener las contiguas.
	 * @return Set de Manzana
	 */
	public Set<Integer> darManzanasContiguasDe(Integer eleccion) 
	{
		Set<Integer> ret = new HashSet<Integer>();
		for(Manzana m : _censo.darVecinosDe(_manzanas.get(eleccion))) 
		{
			ret.add(m.darId());
		}
		return ret;
	}

	/**
	 * Elimina la contiguidad entre dos manzanas.
	 * @param primerManzana 
	 * @param segundaManzana
	 */
	public void eliminarContiguidadDeManzanas(Integer primerManzana, Integer segundaManzana) 
	{
		_censo.eliminarRegistroManzanasVecinas(_manzanas.get(primerManzana),
				_manzanas.get(segundaManzana));
	}
	
	/**
	 * Asigna las manzanas registradas a los censistas registrados.
	 * @return Set de Censistas con asignaci�n.
	 */
	public Set<String> distribuirCensistasEnRadio() {
		Set<String> ret = new HashSet<String>();
		aplicar(_censo.asignarManzanas(), c -> ret.add(c.getNombre()));
		
		return ret;
	}
	
	/**
	 * Consumidor para aplicar lambda.
	 */
	private void aplicar(Set<Censista> censistas, Consumer<Censista> consumidor)
	{
		for(Censista censista : censistas)
			consumidor.accept(censista);
	}
	
	/**
	 * Devuelve un arreglo de n�meros de manzanas asignadas al censista pasado
	 * por par�metro.
	 * @param nombreDeCensista corresponde al censista cuyas manzanas se buscar�n.
	 * @return Object[] que en este caso es un Integer[] de n�meros de manzana.
	 */
	public Object[] darManzanasDeCensista(String nombreDeCensista) 
	{
		return _censistas.get(nombreDeCensista).darManzanas().stream().map(m -> m.darId()).toArray();
	}

	/**
	 * Verifica si el censo tiene registros.
	 * @return true si hay por lo menos una manzana creada,
	 * false en caso contrario.
	 */
	public boolean isEmpty() 
	{
		return _manzanas.isEmpty();
	}
	

	/**
	 * Evalua si los datos fueron guardados corresctamente.
	 * @return true si los datos fueron guardados correctamente, de lo contrario retorna false.
	 */ 
	public boolean guardarDatos() 
	{
		try 
		{
			FileOutputStream fos = new FileOutputStream("ultimoCenso");
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(this);
			out.close();
			return true;
			
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Recupera los datos del �ltimo censo guardado.
	 * @return RegistrosCenso.
	 */
	public static RegistrosCenso recuperarDatos() 
	{
		try 
		{
			FileInputStream fis = new FileInputStream("ultimoCenso");
			ObjectInputStream in = new ObjectInputStream(fis);
			RegistrosCenso op = (RegistrosCenso) in.readObject();
			in.close();
			return op;
			
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
}