package Model;
import java.io.Serializable;
import java.util.Objects;

/**
 * Esta clase modela una manzana a utilizar en un censo. Cada vez que se crea una manzana
 * se le asigna un n�mero que la identifica.
 * @author Caro, Xime
 */
public class Manzana implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;                //n�mero que identifica a la manzana
	private double[] coord;        //coordena "x" e "y"
	private static int proxId = 1; //comienza en 1 y se le va agregando una unidad cada 
								   //vez que se crea una manzana
								   
	public Manzana() {
		this.id = proxId;
		proxId++;
	}
	
	/**
	 * Devuelve el identificador �nico de la manzana.
	 * @return int que representa el id de la manzana.
	 */
	public int darId() 
	{
		return this.id;
	}
	
	/**
	 * Devuelve el id que identificar� a la siguiente manzana creada.
	 * @return int que representa el id de la siguiente manzana.
	 */
	public static int darProximoID() 
	{
		return proxId;
	}
	
	/**
	 * Setea el id de la manzana siguiente por el n�mero pasado por par�metro.
	 * @param num int que representa el id de la pr�xima manzana a crear.
	 */
	public static void setProximoID(int num) 
	{
		proxId = num;
	}
	
	/**
	 * Modifica la coordenada de la manzana por la pasada por par�metro.
	 * @param coord double[] donde el primer elemento es la coordenada en el eje X y el segundo 
	 * en el eje Y.
	 */
	public void setCoord(double[] coord) 
	{
		this.coord = coord;
	}
	
	/**
	 * Devuelve la coordenada de la manzana, si no tiene coordenada devuelve null.
	 * @return  double[] donde el primer elemento es la coordenada en el eje X y el segundo 
	 * en el eje Y.
	 */
	public double[] getCoord() 
	{
		return coord;
	}

	@Override
	public String toString() 
	{
		return "" + id;
	}
	
	@Override
	public boolean equals (Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manzana other = (Manzana) obj;
		return  Objects.equals(id, other.id) && evaluarCoord(this, other);
	}
	
	/**
	 * Evalua si dos manzanas tienen las mismas coordenadas.
	 * @param m1 primer Manzana.
	 * @param m2 segunda Manzana.
	 * @return true si tienen las mismas coordenas y false en caso contrario.
	 */
	private boolean evaluarCoord(Manzana m1, Manzana m2) 
	{
		double[] coorM1 = m1.getCoord();
		double[] coorM2 = m2.getCoord();
		return (coorM1[0] == coorM2[0] && coorM1[1] == coorM2[1]) || 
				(coorM1[0] == coorM2[1] && coorM1[0] == coorM2[1]);
	}
}