package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * Esta clase modela a un censo, compuesto por un conjunto de manzanas vecinas que conforman un
 * radio censal y una lista de censista a los cuales se le asignan las manzanas disponibles 
 * (m�ximo de 3 manzanas por censista).
 * @author Caro, Xime
 */
public class Censo implements Serializable {

	private static final long serialVersionUID = 1L;
	private Grafo<Manzana> radioCensal;                    //Manzanas y sus relaciones de contiguidad
	private ArrayList<Censista> censistasDisponibles;      //Censistas registrados
	private int i;                                         //Indice para avanzar en la lista de censistas
	private LinkedList<Manzana> manzanasPendientes;        //Lista de manzanas usada para asignar censistas.
	private Map<Censista,Set<Manzana>> censistasAsignados; //Censistas con sus manzanas asignadas.
	

	public Censo() 
	{
		this.radioCensal = new Grafo<Manzana>();
		this.censistasDisponibles = new ArrayList<Censista>();
		this.censistasAsignados = new HashMap<Censista, Set<Manzana>>();	
	}
	
	/**
	 * Asigna las manzanas del radio censal a los censistas y 
	 * devuelve un conjunto de censistas asignados.
	 * @return conjunto con todos los censistas a los que se les asign� alguna manzana.
	 */
	public Set<Censista> asignarManzanas() 
	{
		//evalua si ya hubo una asignacion y lo reestablece
		if(!censistasAsignados.isEmpty()) 
		{
			this.reestablecerAsignacion();
		}
		verificarRadioCensalYcensistas();
		inicializarParametrosDeAsignacion();
		Manzana manzanaActual = this.manzanasPendientes.getFirst();
		Censista censistaActual = this.censistasDisponibles.get(i);
		
		//mientras no se asignen todas las manzanas pendientes
		while (!this.manzanasPendientes.isEmpty()) 
		{
			asignarManzanasAcensista(censistaActual, manzanaActual);
			//si hay manzanas pendientes cambia el censista
			if (!this.manzanasPendientes.isEmpty()) 
			{
				manzanaActual = this.manzanasPendientes.getFirst();
				i++;
				verificarDisponibilidadDeCensistas();
				censistaActual = this.censistasDisponibles.get(i);
			}
		}
		return this.censistasAsignados.keySet();
	}
	
	/**
	 * Reestablece las manzanas asignadas para volver a realizar la asignaci�n.
	 */
	private void reestablecerAsignacion() 
	{
		for(Censista c: censistasAsignados.keySet())
			c.eliminarManzanaAsignadas();
		censistasAsignados.clear();
	}
	
	/**
	 * Inicializa las manzanas pendientes y el indice de la lista de censistas.
	 */
	private void inicializarParametrosDeAsignacion() 
	{
		this.manzanasPendientes = new LinkedList<Manzana>();
		completarManzanasPendientesBFS();
		i = 0;
	}
	
	/**
	 * Completa las manzanas pendientes en orden BFS.
	 * @throws IllegalArgumentException si el radio censal no tiene manzanas contiguas.
	 */
	private void completarManzanasPendientesBFS() 
	{
		BFS<Manzana> bfsParaCompletarPendientes = new BFS<Manzana> (this.radioCensal);
		
		if(!bfsParaCompletarPendientes.esConexo()) 
		{
			throw new IllegalArgumentException ("El radio censal no tiene todas sus manzanas contiguas");
		}
		this.manzanasPendientes = bfsParaCompletarPendientes.alcanzables(radioCensal, 
				radioCensal.darUnVertice());
	}
	
	/**
	 * Verifica que el radio censal contenga manzanas y si hay censistas disponibles
	 * para asignar manzanas.
	 * @throws IllegalArgumentException si no hay manzanas registradas.
	 */
	private void verificarRadioCensalYcensistas() 
	{
		if(radioCensal.tamanio()==0)
			throw new IllegalArgumentException ("No hay manzanas registradas en el radio censal");
		verificarDisponibilidadDeCensistas();
	}
	
	/**
	 * Verifica si hay censistas disponibles para asignar manzanas.
	 * @throws IllegalArgumentException si no hay censistas disponibles.
	 */
	private void verificarDisponibilidadDeCensistas() 
	{
		if(this.censistasDisponibles.size() <= i) 
		{
			throw new IllegalArgumentException ("No hay censistas disponibles para "
					+ "asignar las manzanas pendientes.");
		}
	}
	
	/**
	 * Asigna manzanas al censista correspondiente.
	 * @param c Censista al que se le asigna las manzanas.
	 * @param m Manzana a asignar al censista y los vecinos de �sta.
	 */
	private void asignarManzanasAcensista(Censista c, Manzana m) 
	{
		Manzana actual = m;
		asignarUnaManzana(c, actual);
		this.manzanasPendientes.remove(m);
		
		while(darManzanaVecinaDisponible(actual) != null && c.cantidadDeManzanasAsignadas() < 3) 
		{
			Manzana disponible = darManzanaVecinaDisponible(actual);
			asignarUnaManzana(c, disponible);
			this.manzanasPendientes.remove(disponible);
			
			//si la manzana actual no tiene vecinos disponibles
			//busca la manzana vecina con menor cantidad de vecinos disponibles
			if(darManzanaVecinaDisponible(actual)== null) 
			{
				ArrayList<Manzana> vecinos = new ArrayList<Manzana>();
				vecinos.addAll(c.darManzanas());
				ordenarPorMenorCantDeVecinosDisponibles(vecinos);
				for(Manzana manzana: vecinos) {
					
					if(manzana != actual) {
						actual = manzana;
					}
				}
			}
		}
	}

	/**
	 * Asigna la manzana pasada por par�metro al censista correspondiente.
	 * @param c Censista al que se le asigna la manzana.
	 * @param m Manzana que se le asigna al censista.
	 */
	private void asignarUnaManzana(Censista c, Manzana m) 
	{
		c.asignarManzana(m);
		if(!this.censistasAsignados.keySet().contains(c)) 
		{
			Set<Manzana> manzanas = new HashSet<Manzana>();
			manzanas.add(m);
			this.censistasAsignados.put(c, manzanas);
		}
		else 
		{
			this.censistasAsignados.get(c).add(m);
		}
	}
	
	/**
	 * Devuelve una manzana no asignada vecina de la pasada por par�metro 
	 * (con menor n�mero de vecinos disponibles), si no hay devuelve null.
	 * @param manz Manzana a la que se eval�a sus vecinos disponibles.
	 * @return Manzana que corresponde a la manzana vecina con menor n�mero de
	 * manzanas vecinas disponibles.
	 */
	private Manzana darManzanaVecinaDisponible(Manzana manz) 
	{
		ArrayList<Manzana> vecinosEnOrden = new ArrayList<Manzana>();
		vecinosEnOrden.addAll(this.radioCensal.vecinos(manz)) ; //agrega los elementos del set en la lista
		ordenarPorMenorCantDeVecinosDisponibles(vecinosEnOrden);
		for(Manzana m: vecinosEnOrden) {
			if(this.manzanasPendientes.contains(m)) 
			{
				return m;
			}
		}
		return null;
	}
	
	/**
	 * Ordena la lista de manzanas de menos a mayor n�mero de manzanas vecinas
	 * disponibles.
	 * @param lista ArrayList de Manzanas a ordenar.
	 */
	private void ordenarPorMenorCantDeVecinosDisponibles(ArrayList<Manzana> lista) 
	{
		Collections.sort(lista,(m1,m2) -> {return cantVecinosDisponiblesDe(m1)<cantVecinosDisponiblesDe(m2) ? -1 :
			(cantVecinosDisponiblesDe(m1) == cantVecinosDisponiblesDe(m1) ? 0 : 1);});
	}
	
	/**
	 * Devuelve el n�mero de manzanas vecinas disponibles de la 
	 * pasada por par�metro.
	 * @param m Manzana a la cual se calcula sus vecinos disponibles
	 * @return int que representa el n�mero de manzanas vecinas disponibles.
	 */
	private int cantVecinosDisponiblesDe(Manzana m) 
	{
		int ret = 0;
		for(Manzana v: this.radioCensal.vecinos(m)) 
		{
			if (this.manzanasPendientes.contains(v)) 
			{
				ret += 1;
			}
		}
		return ret;	
	}
	 
	/**
	 * Agrega la manzana al censo.
	 * @param m Manzana a registrar
	 */
	public void agregarManzana(Manzana m) 
	{
		this.radioCensal.agregarVertice(m);
	}
	
	/**
	 * Elimina la manzana del censo.
	 * @param m Manzana a eliminar.
	 */
	public void eliminarManzana(Manzana m) 
	{
		this.radioCensal.eliminarVertice(m);
		for(Censista c: this.censistasAsignados.keySet())
		{
			if(c.darManzanas().contains(m)) 
			{
				c.eliminarManzana(m);
				censistasAsignados.get(c).remove(m);
			}
		}
	}	
	
	/**
	 * Registra manzanas contiguas en el radio censal del censo.
	 * @param m1 primera manzana de la relaci�n.
	 * @param m2 segunda manzana de la relaci�n.
	 * @throws IllegalArgumentException si las manzanas ya est�n registradas como contiguas.
	 */
	public void registrarManzanasVecinas(Manzana m1, Manzana m2) 
	{
		if(this.radioCensal.existeArista(m1, m2))
			throw new IllegalArgumentException("Las manzanas ya estan registradas como vecinas");
		else 
			this.radioCensal.agregarArista(m1, m2);	
	}
	
	/**
	 * Elimina un registro de manzanas contiguas.
	 * @param m1 primera manzana de la relaci�n.
	 * @param m2 segunda manzana de la relaci�n.
	 * @throws IllegalArgumentException si alguna de las manzanas no est�n registradas,
	 */
	public void eliminarRegistroManzanasVecinas(Manzana m1, Manzana m2) 
	{
		if (!this.radioCensal.existeArista(m1, m2))
			throw new IllegalArgumentException("Las manzanas no estan registradas como vecinas");
		else
			this.radioCensal.eliminarArista(m1, m2);
	}
	
	/**
	 * Eval�a si la manzana est� registrada.
	 * @param m Manzana
	 * @return true si la manzana est� registrada y false en caso contrario.
	 */
	public boolean esManzanaRegistrada(Manzana m) 
	{
		return radioCensal.existeVertice(m);
	}

	/**
	 * Registra un censista al censo.
	 * @param c Censista a registrar.
	 */
	public void registrarCensista(Censista c) 
	{
		if(!this.censistasDisponibles.contains(c))
			this.censistasDisponibles.add(c);
	}
	
	/**
	 * Elimina un censista del censo.
	 * @param c Censista a eliminar.
	 * @throws IllegalArgumentException si el censista no est� registrado.
	 */
	public void eliminarCensista(Censista c) 
	{
		if(censistasAsignados.keySet().contains(c)) 
		{
			Set<Manzana> manzanasDeC = c.darManzanas();
			censistasDisponibles.remove(c);
			censistasAsignados.remove(c);
			this.manzanasPendientes.addAll(manzanasDeC);
			
		}
		else if (censistasDisponibles.contains(c)) {
			censistasDisponibles.remove(c);
		}
		else
		{
			throw new IllegalArgumentException("El cencista no est� registrado");
		}
	}
	
	/**
	 * Eval�a si el censista esta registrado en el censo.
	 * @param c Censista a evaluar.
	 * @return true si esta registrado y false en caso contrario.
	 */
	public boolean esCensistaRegistrado(Censista c) 
	{
		return censistasDisponibles.contains(c);
	}
	
	/**
	 * Devuelve el conjunto de manzanas contiguas de la manzana correspondiente.
	 * @param m Manzana de la que se obtiene sus vecinos.
	 * @return Set de Manzanas.
	 */
	public Set<Manzana> darVecinosDe(Manzana m) 
	{
		return this.radioCensal.vecinos(m);
	}
	
}