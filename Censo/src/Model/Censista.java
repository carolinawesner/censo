package Model;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
/**
 * Esta clase modela a un censista que contiene un nombre y un conjunto de manzanas asignadas.
 * @author Caro, Xime
 */
public class Censista implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nombre;
	private Set<Manzana> manzanas;
	
	public Censista(String nombre) 
	{
		if (nombre.isBlank() || nombre.isEmpty())
			throw new IllegalArgumentException("El nombre no es correcto");
		else 
		{
			this.nombre = nombre;
			this.manzanas = new HashSet<Manzana>();
		}
	}
	
	/**
	 * Devuelve el nombre del censista.
	 * @return String que representa el nombre del censista.
	 */
	public String getNombre() 
	{
		return this.nombre;
	}
	
	/**
	 * Asigna una manzana al censista.
	 * @param m Manzana a asignar.
	 */
	public void asignarManzana(Manzana m) 
	{
		if (!this.manzanas.contains(m))
			this.manzanas.add(m);
		else
			throw new IllegalArgumentException("La manzana ya fue asignada");
	}
	
	/**
	 * Devuelve la cantidad de manzanas asignadas.
	 * @return int que representa el n�mero de manzanas asignadas.
	 */
	public int cantidadDeManzanasAsignadas() 
	{
		return this.manzanas.size();
	}
	
	/**
	 * Elimina las manzanas asignadas.
	 */
	public void eliminarManzanaAsignadas() 
	{
		manzanas.clear();
	}
	
	/**
	 * Elimina la manzana pasada por par�matro.
	 * @param m Manzana a eliminar.
	 */
	public void eliminarManzana(Manzana m) 
	{
		if(this.manzanas.contains(m))
			manzanas.remove(m);
	}
	
	/**
	 * Devuelve el conjunto de manzanas asignadas.
	 * @return Set de Manzana que representa a todas las manzanas asignadas.
	 */
	public Set<Manzana> darManzanas() 
	{
		return this.manzanas;
	}
	
	@Override
	public String toString() 
	{
		String ret = "Censista " + nombre + " Manzanas: [ ";
		for (Manzana m: this.manzanas) {
			ret = ret + m.darId() + " "; 
		}
		return ret + "]";
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Censista other =  (Censista) obj;
		return Objects.equals(this.nombre, other.nombre) && verificarManzanas(other);
	}
	
	/**
	 * Eval�a si el censista pasado por par�metro tiene las mismas manzanas asignadas que 
	 * el censista correspondiente.
	 * @param o Censista a comparar.
	 * @return true si las manzanas coinciden y false en caso contrario.
	 */
	private boolean verificarManzanas(Censista o) 
	{
		if(this.cantidadDeManzanasAsignadas() != o.cantidadDeManzanasAsignadas())
			return false;
		for (Manzana m: this.darManzanas()) 
		{
			if(!o.darManzanas().contains(m))
				return false;
		}
		return true;
	}
}
