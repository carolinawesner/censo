package Model;
import java.util.LinkedList;

/**
 * Esta clase implementa el algoritmo BFS sobre un grafo determinado. 
 * @author Caro, Xime
 * @param <T> tipo generico
 */
public class BFS<T> {
	
	private Grafo<T> grafo;                   //grafo al que se le aplica el algoritmo
	private LinkedList<T> verticesPendientes; //Lista de v�rtices a evaluar.
	private LinkedList<T> verticesMarcados;   //Lista de v�rtices evaluados.
	
	/**
	 * Constructor de la clase.
	 * @param g grafo de tipo gen�rico.
	 * @throws IllegalArgumentException si el grafo es nulo.
	 */
	public BFS(Grafo<T> g) throws IllegalArgumentException 
	{
		if (g == null)
			throw new IllegalArgumentException("El grafo no puede ser nulo");
		this.grafo = g;
	}
	
	/**
	 * Eval�a si el grafo correspondiente es conexo.
	 * @return true si es conexo, false en caso contrario
	 */
	public boolean esConexo() 
	{
		if (grafo.tamanio()==0)
			return true;
		return alcanzables(grafo, grafo.darUnVertice()).size() == grafo.tamanio();
	}
	
	/**
	 * Retorna una lista de todos los v�rtices alcanzables en un grafo, 
	 * desde un v�rtice origen.
	 * @param g Grafo de tipo gen�rico.
	 * @param origen v�rtice de tipo gen�rico 
	 * @return LinkedList de tipo gen�rico con los v�rtices alcanzables desde el origen.
	 */
	public LinkedList<T> alcanzables(Grafo<T> g, T origen) 
	{
		//conjunto de vertices alcanzables
		LinkedList<T> ret = new LinkedList<T>();
		inicializarBusqueda(g, origen);
			
		while (verticesPendientes.size() > 0) 
		{
			T v = seleccionarYMarcarVertice(ret);
			//agrega los vecinos no marcados de dicho vertice
			agregarVecinosNoMarcados(g, v);
			//elimina el vertice de los pendientes
			removerSeleccionado(v);
		}
		return ret;
	}
	
	/**
	 * Inicializa los datos necesarios para ejecutar el algoritmo.
	 * @param g grafo de tipo gen�rico
	 * @param origen v�rtice de tipo gen�rico del cual se iniciar� el recorrido
	 */
	private void inicializarBusqueda(Grafo<T> g, T origen) 
	{
		verticesPendientes = new LinkedList<T>();
		verticesPendientes.addFirst(origen);
		verticesMarcados = new LinkedList<T>();
	}
	
	/**
	 * Selecciona un v�rtice del conjunto de pendientes, lo agrega al conjunto
	 * pasado como par�metro y retorna el v�rtice elejido.
	 * @param verticesMarcados2 conjunto de tipo generico.
	 * @return v�rtice seleccionado-
	 */
	private T seleccionarYMarcarVertice(LinkedList<T> verticesMarcados2) 
	{
		//selecciona un vertice de los pendientes
		T v = verticesPendientes.getFirst();
		//Lo agrega a marcados
		verticesMarcados.add(v);
		//lo agrega al conjunto pasado como par�metro
		verticesMarcados2.add(v);
		//retorna el vertice elegido
		return v;
	}

	/**
	 * 
	 * Recorre los vecinos del v�rtice especificado y agrega al conjunto de pendientes 
	 * aquellos que no fueron marcados.
	 * @param g grafo de tipo gen�rico
	 * @param i v�rtice a evaluar su vecinos pendientes
	 */
	private void agregarVecinosNoMarcados(Grafo<T> g, T i) 
	{
		for (T vecino: g.vecinos(i)) 
		{
			if (!verticesMarcados.contains(vecino) && !verticesPendientes.contains(vecino)) 
			{
				verticesPendientes.addLast(vecino);
			}
		}
	}
	
	/**
	 * Elimina el v�rtice pasado por par�metro del conjunto de pendientes
	 * @param v v�rtice que se quiere eliminar.
	 */
	private void removerSeleccionado(T v) 
	{
		verticesPendientes.remove(v);
	}
}