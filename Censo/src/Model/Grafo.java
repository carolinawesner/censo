package Model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Esta clase modela un Grafo con v�rtices de tipo G�nerico.
 * @param <T> tipo de objeto de sus v�rtices.
 * @author Caro, Xime
 */
public class Grafo <T> implements Serializable 
{

	private static final long serialVersionUID = 1L;
	private Map <T, Set<T>> vecinosPorVertice; //V�rtices con sus vecinos correspondientes

	/**
	 * Crea un grafo vac�o.
	 */
	public Grafo() 
	{
		vecinosPorVertice = new HashMap<T, Set<T>>();
	}
	
	/**
	 * Agrega un v�rtice no existente al grafo.
	 * @param i corresponde a un v�rtice de tipo gen�rico.
	 * @throws IllegalArgumentException si el grafo ya contiene al v�rtice.
	 */
	public void agregarVertice(T i) throws IllegalArgumentException 
	{
		if (existeVertice(i))
			throw new IllegalArgumentException("El elemento ya existe");
		else 
		{
			Set<T> vecinos = new HashSet<T>();
			vecinosPorVertice.put(i, vecinos);
		}
	}
	
	/**
	 * Eval�a si existe el v�rtice pasado por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice del grafo.
	 * @return true si el v�rtice esta registrado en el grafo y false en caso contrario.
	 */
	public boolean existeVertice(T i) 
	{
		return vecinosPorVertice.keySet().contains(i);
	}
	
	/**
	 * Elimina un v�rtice existente del grafo.
	 * @param i corresponde a un v�rtice de tipo gen�rico.
	 * @throws IllegalArgumentException si el grafo no contiene al v�rtice.
	 */
	public void eliminarVertice(T i) throws IllegalArgumentException 
	{
		if (!existeVertice(i))
			throw new IllegalArgumentException("El elemento no existe");
		else 
		{
			Set<T> vecinosVerticeAEliminar = vecinosPorVertice.get(i);
			for(T a : vecinosVerticeAEliminar) 
			{
				vecinosPorVertice.get(a).remove(i);
			}
			vecinosPorVertice.remove(i);
		}
	}
	
	/**
	 * Devuelve el tama�o del grafo.
	 * @return int que corresponde a la cantidad de v�rtices del grafo.
	 */
	public int tamanio()
	{
		return vecinosPorVertice.size();
	}
		
	/**
	 * Agrega una arista inexistente al grafo. Si alguno de los v�rtices no existen,
	 * los registra y luego agrega la arista.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice de la arista.
	 * @throws IllegalArgumentException si la arista ya est� registrada.
	 */
	public void agregarArista(T i, T j) throws IllegalArgumentException 
	{
		if(!existeVertice(i)) agregarVertice(i);

		if(!existeVertice(j)) agregarVertice(j);
		
		if (existeArista(i, j))
			throw new IllegalArgumentException("La arista ya existe");
		
		verificarDistintos(i, j);
		vecinosPorVertice.get(i).add(j);
		vecinosPorVertice.get(j).add(i);
	}
	
	/**
	 * Elimina una arista existente del grafo.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice de la arista.
	 * @throws IllegalArgumentException si la arista no esta registrada.
	 */
	public void eliminarArista(T i, T j) throws IllegalArgumentException 
	{
		if (!existeArista(i, j)) 
		{
			throw new IllegalArgumentException("La arista no existe");
		}
		verificarDistintos(i, j);
		vecinosPorVertice.get(i).remove(j);
		vecinosPorVertice.get(j).remove(i);
	}
	
	/**
	 * Eval�a si existe la arista que conecta los v�rtices recibidos por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice de la arista.
	 * @return true si la arista existe en el grafo y false en caso contrario.
	 * @throws IllegalArgumentException si los v�rtices no estan registrados o 
	 * son el mismo objeto.
	 */
	public boolean existeArista(T i, T j) throws IllegalArgumentException
	{
		if(!existeVertice(i) || !existeVertice(j) )
			throw new IllegalArgumentException("Los v�rtices deben estar registrados.");
		
		verificarDistintos(i, j);
		return vecinosPorVertice.get(i).contains(j) && vecinosPorVertice.get(j).contains(i);
	}

	/**
	 * Verifica que los v�rtices pasados por par�metro son distintos.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice del grafo.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice del grafo.
	 * @throws IllegalArgumentException si los v�rtices son el mismo objeto.
	 */
	private void verificarDistintos(T i, T j)throws IllegalArgumentException
	{
		if(i.equals(j) )
			throw new IllegalArgumentException("No se permite conexion entre los mismos elementos");
	}
		
	/**
	 * Devuelve el conjunto de vecinos de un v�rtice pasado por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice del grafo.
	 * @return Set de tipo gen�rico que corresponde al conjunto de 
	 * v�rtices conectados al v�rtice correspondiente.
	 * @throws IllegalArgumentException si el v�rtice no pertenece al grafo.
	 */
	public Set<T> vecinos(T i) throws IllegalArgumentException
	{
		if (!existeVertice(i)) 
		{
			throw new IllegalArgumentException("El elemento no existe");
		}
		return vecinosPorVertice.get(i);		
	}
		
	/**
	 * Devuelve el conjunto de v�rtices del grafo.
	 * @return Set de tipo gen�rico que corresponde al conjunto de v�rtices del grafo.
	 */
	public Set<T> darVertices() 
	{
		return vecinosPorVertice.keySet();
	}
		
	/**
	 * Devuelve un v�rtice aleatorio del grafo.
	 * @return v�rtice del grafo.
	 * @throws IllegalArgumentException si el grafo no tiene v�rtices (est� vac�o).
	 */
	public T darUnVertice() throws IllegalArgumentException 
	{
		if (vecinosPorVertice.keySet().isEmpty())
			throw new IllegalArgumentException("El grafo no tiene v�rtices.");
		
		for(T elem : vecinosPorVertice.keySet())
			return elem;
		return null;	
	}
	
	@Override
	public String toString () 
	{
		if(vecinosPorVertice == null) return null;
		String s = "";
		for (T i : vecinosPorVertice.keySet()) 
		{
			s = s + "Elemento [" + i.toString() + "] ";
			s = s + "Se relaciona con " + this.vecinos(i).toString() +"\n";
		}
		return s;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Grafo<T> other =  (Grafo<T>) obj;
		return Objects.equals(darVertices(), other.darVertices()) && verificarVecinos(other);
	}
	
	/**
	 * Recibe un grafo como par�metro y verifica que tenga 
	 * los mismos vecinos que el grafo correpondiente.
	 */
	private boolean verificarVecinos(Grafo<T> g) 
	{
		for (T a : this.darVertices()) 
		{
			if(this.vecinos(a).size() != g.vecinos(a).size())
				return false;
			for(T v: this.vecinos(a))
				if(!g.vecinos(a).contains(v))
					return false;
		}
		return true;
	}
}